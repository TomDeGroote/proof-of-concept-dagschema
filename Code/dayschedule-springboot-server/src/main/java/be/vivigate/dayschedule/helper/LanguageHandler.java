package be.vivigate.dayschedule.helper;

import be.vivigate.dayschedule.DayscheduleApplication;
import be.vivigate.dayschedule.WebRestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Component
@ConfigurationProperties(prefix="languagehandler")
public class LanguageHandler {

    private static final Logger logger = LoggerFactory.getLogger(WebRestController.class);

    private enum Language {
        EN("eng.xml"), NL("nl.xml");

        private String fileName;

        Language(String fileName) {
            this.fileName = fileName;

        }

        public InputStream getLanguageInputStream() {
            String fileLocation = "../../../static/languages" + File.separator + fileName;
            return DayscheduleApplication.class.getResourceAsStream(fileLocation);
        }

        @Override
        public String toString() {
            return fileName.substring(0, 2);
        }
    }

    private static Language language;

    public static Language getLanguage() {
        return language;
    }

    public static String getLanguageString() {
        return language.toString();
    }

    public static void setLanguage(Language language) {
        LanguageHandler.language = language;
    }

    public static String getString(String id) {
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document document = docBuilder.parse(language.getLanguageInputStream());

            NodeList nodeList = document.getElementsByTagName("*");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    // do something with the current element
                    if (node.getNodeName().equals(id)) {
                        return node.getTextContent();
                    }
                }
            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            logger.error("getString - Error while getting language string: ", e);
        }
        logger.info("getString - Returning undefined");
        return "undefined";
    }

}
