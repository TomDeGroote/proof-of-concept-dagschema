package be.vivigate.dayschedule.widgetbar;

import be.vivigate.dayschedule.person.Person;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.nio.file.Path;

@Component
public class CaregiverStatus extends Widget {
    private Person caregiver;

    public CaregiverStatus() {
        super();
    }

    public Person getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Person caregiver) {
        this.caregiver = caregiver;
    }
}
