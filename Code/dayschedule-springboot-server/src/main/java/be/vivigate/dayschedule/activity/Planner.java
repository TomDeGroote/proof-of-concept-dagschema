package be.vivigate.dayschedule.activity;

import be.vivigate.dayschedule.WebRestController;
import be.vivigate.dayschedule.helper.GoogleHandler;
import be.vivigate.dayschedule.helper.Time;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

@Component
@ConfigurationProperties(prefix="planner")
public class Planner {
    private Set<String> existingActivities = new HashSet<>();
    private Map<Time, List<Activity>> activities = new HashMap<>();
    private List<Activity> recurringActivities = new ArrayList<>();
    private List<String> calendars;

    private static final Logger logger = LoggerFactory.getLogger(WebRestController.class);

    // TODO what if a recurrent event is eddited for one time and not for all events?
    private void addActivity(Activity activity) {
        if (existingActivities.contains(activity.getUniqueID())) {
            logger.info("Updating activity (expect a remove as well): " + activity.getUniqueID());
            removeActivity(activity.getUniqueID());
        } else {
            logger.info("Adding activity: " + activity.getUniqueID() + ", start: " + activity.getStartTime() + ", end: " + activity.getEndTime());
        }
        existingActivities.add(activity.getUniqueID());

        if (activity.isRecurrent()) {
//            recurringActivities.add(activity); TODO enable recurring activities!
        } else {
            activities.computeIfAbsent(activity.getStartDate(), k -> new ArrayList<>());
            activities.get(activity.getStartDate()).add(activity);
        }
    }

    private void removeActivity(String id) {
        logger.info("Removing activity: " + id);
        existingActivities.remove(id);

        // Check all normal activities
        Set<Time> dates = activities.keySet();
        for (Time date : dates) {
            List<Activity> deleteFrom = activities.get(date);
            for (int i = 0; i < deleteFrom.size(); i++) {
                if (deleteFrom.get(i).getUniqueID().equals(id)) {
                    deleteFrom.remove(i);
                    return;
                }
            }
        }

        // Check all recurring activities
        for (int i = 0; i < recurringActivities.size(); i++) {
            if (recurringActivities.get(i).getUniqueID().equals(id)) {
                recurringActivities.remove(i);
                return;
            }
        }
    }

    public ActivityList getActivitiesForDate(long dateMillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(dateMillis);
        Time time = new Time(0, 0, cal.get(Calendar.DAY_OF_YEAR), cal.get(Calendar.YEAR));
        List<Activity> result = new ArrayList<>();

        // Get all activities that match representation, Iterator used to avoid ConcurrentModificationError
        if (activities.get(time) != null) {
            for (Iterator<Activity> activitiesIterator = activities.get(time).iterator(); activitiesIterator.hasNext(); ) {
                Activity activity = activitiesIterator.next();
                if (activity.getStartTime().isSameDate(dateMillis)) {
                    result.add(activity);
                }
            }
        }

        // Also check recurrent activities
        for (Activity activity : recurringActivities) {
            if(activity.occursOnDate(time.getYear(), time.getDayOfYear())) {
                Activity singleEventActivity = activity.duplicateForDate(time.getYear(), time.getDayOfYear());
                result.add(singleEventActivity);
            }
        }

        // Now sort according to the time they take place
        logger.info("Asked for: " + dateMillis + " " + new Date(dateMillis));
        Collections.sort(result);
        for (Activity act : result) {
            logger.info("getActivitiesForDate " + new Date(dateMillis) + "-> " + act.getUniqueID() + "\t - \t" +
                    act.getStartTime() + "\t - \t" +
                    act.getEndTime() + "\t - \t" +
                    act.getRecurrence() + "\t - \t" +
                    act.getTitle());
        }

        return new ActivityList(result);
    }

    public List<String> getCalendars() {
        return calendars;
    }

    public void setCalendars(List<String> calendars) {
        this.calendars = calendars;
    }

    @Scheduled(fixedRate=60000)
    public void retrieveActivities() {
        logger.info("Retrieving activities...");
        try {
            for (String calendarName : getCalendars()) {
                loadActivities(GoogleHandler.getCalendarEvents(calendarName), calendarName);
            }
        } catch (IOException e) {
            logger.error("retrieveActivities: ", e);
        }
    }

    private void loadActivities(List<Event> events, String calendarName) {
        for (Event event : events) {
            if (event.getStatus().equals("cancelled")) {
                removeActivity(event.getId());
            } else if (event.getStatus().equals("confirmed")) {
                Activity activity = activityFromEvent(event, calendarName);
                addActivity(activity);
            } else {
                logger.info("Event not added due to bad status!");
            }
        }
    }

    private Activity activityFromEvent(Event event, String calendarName) {
        Activity activity = new Activity();

        activity.setUniqueID(event.getId());
        activity.setTitle (event.getSummary());
        activity.setDescription(event.getDescription());
        activity.setActivityKind(ActivityKind.PLANNED);
        // Set start and end date of activity
        if(event.getStart().getDate() != null) {
            // Whole day
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(event.getStart().getDate().getValue()));
            // TODO keep getTimeZoneShift() in mind!
            activity.setStartTime(new Time(0, 0, cal.get(Calendar.DAY_OF_YEAR), cal.get(Calendar.YEAR)));
            activity.setEndTime(new Time(23, 59, cal.get(Calendar.DAY_OF_YEAR), cal.get(Calendar.YEAR)));
        }
        if(event.getStart().getDateTime() != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(event.getStart().getDateTime().getValue()));
            // TODO keep getTimeZoneShift() in mind!
            activity.setStartTime(new Time(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.DAY_OF_YEAR), cal.get(Calendar.YEAR)));
        }
        if(event.getEnd().getDateTime() != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(event.getEnd().getDateTime().getValue()));
            // TODO keep getTimeZoneShift() in mind!
            activity.setEndTime(new Time(cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.DAY_OF_YEAR), cal.get(Calendar.YEAR)));
        }
        activity.setCalendarName(calendarName);
        activity.setColor(event.getColorId());

        // retrieve attachments
        if (event.getAttachments() != null) {
            for (int i = 0; i < event.getAttachments().size(); i++) {
                EventAttachment ea = event.getAttachments().get(i);
                if (ea.getTitle().contains("icoon")) {
                    activity.setIcon("https://docs.google.com/uc?id=" + ea.getFileId());
                } else if (ea.getTitle().contains("afbeelding")) {
                    activity.setImage("https://docs.google.com/uc?id=" + ea.getFileId());
                }
            }
        }

        activity.setRecurrence(event.getRecurrence());
        return activity;
    }
}
