package be.vivigate.dayschedule.activity;

public enum ActivityKind {
    PLANNED, SUGGESTED
}
