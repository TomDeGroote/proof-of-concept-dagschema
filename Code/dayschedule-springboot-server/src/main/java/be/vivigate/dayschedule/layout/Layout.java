package be.vivigate.dayschedule.layout;

import be.vivigate.dayschedule.widgetbar.Widgetbar;

import java.util.HashMap;

public class Layout {

    private String layoutName;
    private HashMap<String, String> layoutElements;
    private Widgetbar widgetbar;

    public String getLayoutName() {
        return layoutName;
    }

    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
    }

    public HashMap<String, String> getLayoutElements() {
        return layoutElements;
    }

    public void setLayoutElements(HashMap<String, String> layoutElements) {
        this.layoutElements = layoutElements;
    }

    public Widgetbar getWidgetbar() {
        return widgetbar;
    }

    public void setWidgetbar(Widgetbar widgetbar) {
        this.widgetbar = widgetbar;
    }
}
