package be.vivigate.dayschedule.helper;

import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Time {
    private int hour;
    private int minute;
    private int dayOfYear;
    private int year;

    public Time(int hour, int minute, int dayOfYear, int year) {
        this.hour = hour;
        this.minute = minute;
        this.dayOfYear = dayOfYear;
        this.year = year;
    }

    public Time(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        this.hour = cal.get(Calendar.HOUR_OF_DAY);
        this.minute = cal.get(Calendar.MINUTE);
        this.dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
        this.year = cal.get(Calendar.YEAR);
    }

    public Time(Calendar cal) {
        this.hour = cal.get(Calendar.HOUR_OF_DAY);
        this.minute = cal.get(Calendar.MINUTE);
        this.dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
        this.year = cal.get(Calendar.YEAR);
    }

    public Time(long datemillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(datemillis);
        this.hour = cal.get(Calendar.HOUR_OF_DAY);
        this.minute = cal.get(Calendar.MINUTE);
        this.dayOfYear = cal.get(Calendar.DAY_OF_YEAR);
        this.year = cal.get(Calendar.YEAR);
    }

    // Looks only if startDate is the same as dateMillis
    public boolean isSameDate(long dateMillis) {
        Calendar checkDate = Calendar.getInstance();
        checkDate.setTimeInMillis(dateMillis);

        if (checkDate.get(Calendar.DAY_OF_YEAR) == this.dayOfYear
                && checkDate.get(Calendar.YEAR) == this.year) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Time time = (Time) o;
        return hour == time.hour &&
                minute == time.minute &&
                dayOfYear == time.dayOfYear &&
                year == time.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hour, minute, dayOfYear, year);
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getDayOfYear() {
        return dayOfYear;
    }

    public int getYear() {
        return year;
    }

    public boolean after(long datemillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(datemillis);

        if (this.year > cal.get(Calendar.YEAR)) {
            return true;
        } else if (this.year < cal.get(Calendar.YEAR)) {
            return false;
        } else {
            if (this.dayOfYear > cal.get(Calendar.DAY_OF_YEAR)) {
                return true;
            } else if (this.dayOfYear < cal.get(Calendar.DAY_OF_YEAR)) {
                return false;
            } else {
                if (this.hour > cal.get(Calendar.HOUR_OF_DAY)) {
                    return true;
                } else if (this.hour < cal.get(Calendar.HOUR_OF_DAY)) {
                    return false;
                } else {
                    if (this.minute > cal.get(Calendar.MINUTE)) {
                        return true;
                    } else if (this.minute < cal.get(Calendar.MINUTE)) {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public boolean before(long datemillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(datemillis);

        if (this.year < cal.get(Calendar.YEAR)) {
            return true;
        } else if (this.year > cal.get(Calendar.YEAR)) {
            return false;
        } else {
            if (this.dayOfYear < cal.get(Calendar.DAY_OF_YEAR)) {
                return true;
            } else if (this.dayOfYear > cal.get(Calendar.DAY_OF_YEAR)) {
                return false;
            } else {
                if (this.hour < cal.get(Calendar.HOUR_OF_DAY)) {
                    return true;
                } else if (this.hour > cal.get(Calendar.HOUR_OF_DAY)) {
                    return false;
                } else {
                    if (this.minute < cal.get(Calendar.MINUTE)) {
                        return true;
                    } else if (this.minute > cal.get(Calendar.MINUTE)) {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public boolean equal(long datemillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(datemillis);

        if (this.year == cal.get(Calendar.YEAR)
                && this.dayOfYear == cal.get(Calendar.DAY_OF_YEAR)
                && this.hour == cal.get(Calendar.HOUR_OF_DAY)
                && this.minute == cal.get(Calendar.MINUTE)) {
            return true;
        } else {
            return false;
        }
    }

    public Date getDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, this.year);
        cal.set(Calendar.DAY_OF_YEAR, this.dayOfYear);
        cal.set(Calendar.HOUR_OF_DAY, this.hour);
        cal.set(Calendar.MINUTE, this.minute);
        return cal.getTime();
    }

    public long getMillis() {
        return this.getDate().getTime();
    }

    @Override
    public String toString() {
        return hour + ":" + minute + " " + dayOfYear + "-" + year;
    }
}
