package be.vivigate.dayschedule.activity;

import java.util.List;

public class ActivityList {

    private List<Activity> activities;

    public ActivityList(List<Activity> activities) {
        this.activities = activities;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}
