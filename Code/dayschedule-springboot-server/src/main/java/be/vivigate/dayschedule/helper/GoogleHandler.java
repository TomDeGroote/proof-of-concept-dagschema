package be.vivigate.dayschedule.helper;

import be.vivigate.dayschedule.DayscheduleApplication;
import be.vivigate.dayschedule.WebRestController;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

public class GoogleHandler {

    private static final Logger logger = LoggerFactory.getLogger(WebRestController.class);

    /** Directory to store user credentials for this application. */
    private static final java.io.File DATA_STORE_DIR = new java.io.File(System.getProperty("user.home"), ".credentials/calendar-java-quickstart");


    /** Global instance of the HTTP transport. */
    private static HttpTransport HTTP_TRANSPORT;

    /** Global instance of the scopes required by this quickstart.
     *
     * If modifying these scopes, delete your previously saved credentials
     * at ~/.credentials/calendar-java-quickstart
     */
    private static final List<String> SCOPES = Arrays.asList(CalendarScopes.CALENDAR_READONLY);

    /** Global instance of the {@link FileDataStoreFactory}. */
    private static FileDataStoreFactory DATA_STORE_FACTORY;

    private static Map<String, String> syncTokens = new HashMap();

    static {
        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
        } catch (Throwable t) {
            logger.error("Error initializing google handler: ", t);
            System.exit(1);
        }
    }

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static Credential authorize() throws IOException {
        // Load client secrets.
        InputStream in = DayscheduleApplication.class.getResourceAsStream("/client_secret.json");

        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow =
                new GoogleAuthorizationCodeFlow.Builder(
                        HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                        .setDataStoreFactory(DATA_STORE_FACTORY)
                        .setAccessType("offline")
                        .build();
        LocalServerReceiver.Builder lsr = new LocalServerReceiver.Builder();
        lsr.setPort(42959);
        Credential credential = new AuthorizationCodeInstalledApp(flow, lsr.build()).authorize("user");
        return credential;
    }

    /**
     * Build and return an authorized Calendar client service.
     * @return an authorized Calendar client service
     * @throws IOException
     */
    private static Calendar getCalendarService() throws IOException {
        Credential credential = authorize();
        return new Calendar.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(DayscheduleApplication.APPLICATION_NAME)
                .build();
    }

    private static Map<String, String> getCalendarTitlesWithID(Calendar service) {
        Map<String, String> calendarIdTitle = new HashMap<>();

        try {
            // Iterate through entries in calendar list and add them to a hashmap
            String pageToken = null;
            do {
                CalendarList calendarList = service.calendarList().list().setPageToken(pageToken).execute();
                List<CalendarListEntry> items = calendarList.getItems();

                for (CalendarListEntry calendarListEntry : items) {
                    calendarIdTitle.put(calendarListEntry.getSummary(), calendarListEntry.getId());
                }
                pageToken = calendarList.getNextPageToken();
            } while (pageToken != null);
        } catch (IOException e) {
            logger.error("getCalendarTitlesWithID: ", e);
        }
        return calendarIdTitle;
    }

    public static List<Event> getCalendarEvents(String calendarName) throws IOException {
        logger.info("getCalendarEvents - Getting activities for calendar: " + calendarName);
        Calendar service = getCalendarService();
        Map<String, String> calendarTitlesWithID = getCalendarTitlesWithID(service);

        java.util.Calendar oneYearAgo = java.util.Calendar.getInstance();
        oneYearAgo.set(java.util.Calendar.YEAR, oneYearAgo.get(java.util.Calendar.YEAR) - 1);
        java.util.Calendar oneYearAhead = java.util.Calendar.getInstance();
        oneYearAhead.set(java.util.Calendar.YEAR, oneYearAhead.get(java.util.Calendar.YEAR) + 1);

        // Prepare the request for the Google Calendar API.
        List<Event> eventList = new ArrayList<>();
        Calendar.Events.List request = service.events().list(calendarTitlesWithID.get(calendarName));
//                .setTimeMin(new DateTime(oneYearAgo.getTime()));
//                .setTimeMax(new DateTime(oneYearAhead.getTime()))
//                .setSingleEvents(true);
        // vTODO no minimum and maximum for sync token, didn't work, look into this -- NOT FOR THESIS

        // Initial full sync
        String syncToken = syncTokens.get(calendarName);
        if (syncToken == null) {
            logger.info("getCalendarEvents - Performing full sync...");
        } else {
            logger.info("getCalendarEvents - Performing updating sync...");
            request.setSyncToken(syncToken);
        }

        String pageToken = null;
        Events events = null;
        do {
            request.setPageToken(pageToken);

            try {
                events = request.execute();
            } catch (GoogleJsonResponseException e) {
                if (e.getStatusCode() == 410) {
                    // A 410 status code, "Gone", indicates that the sync token is invalid.
                    logger.info("getCalendarEvents - Invalid sync token and re-syncing.");
                    syncTokens.remove(syncToken);
                    // Re-execute to do the full sync
                    return getCalendarEvents(calendarName);
                } else {
                    logger.error("getCalendarEvents - error with token: ", e);
                }
            }

            eventList.addAll(events.getItems());
            pageToken = events.getNextPageToken();
        } while (pageToken != null);

        syncTokens.put(calendarName, events.getNextSyncToken());
        logger.info("getCalendarEvents - Sync complete! New tokens: " + syncTokens);
        return eventList;
    }
}
