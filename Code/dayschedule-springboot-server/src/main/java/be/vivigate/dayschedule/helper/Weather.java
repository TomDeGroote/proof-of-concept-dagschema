package be.vivigate.dayschedule.helper;

public class Weather {
    private String icon;
    private String description;

    public Weather(String icon, String description) {
        this.icon = icon;
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public String getDescription() {
        return description;
    }
}
