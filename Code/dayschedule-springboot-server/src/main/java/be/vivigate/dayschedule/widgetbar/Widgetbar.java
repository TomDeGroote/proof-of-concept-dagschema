package be.vivigate.dayschedule.widgetbar;

import org.springframework.stereotype.Component;

@Component
public class Widgetbar {

    private CaregiverStatus caregiverStatus;
    private Clock clock;
    private Message message;
    private PictureWidget pictureWidget;
    private Volunteers volunteers;
    private WeatherWidget weatherWidget;
    private CalendarWidget calendarWidget;

    public CaregiverStatus getCaregiverStatus() {
        return caregiverStatus;
    }

    public void setCaregiverStatus(CaregiverStatus caregiverStatus) {
        this.caregiverStatus = caregiverStatus;
    }

    public Clock getClock() {
        return clock;
    }

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public PictureWidget getPictureWidget() {
        return pictureWidget;
    }

    public void setPictureWidget(PictureWidget picture) {
        this.pictureWidget = picture;
    }

    public Volunteers getVolunteers() {
        return volunteers;
    }

    public void setVolunteers(Volunteers volunteers) {
        this.volunteers = volunteers;
    }

    public WeatherWidget getWeatherWidget() {
        return weatherWidget;
    }

    public void setWeatherWidget(WeatherWidget weatherWidget) {
        this.weatherWidget = weatherWidget;
    }

    public CalendarWidget getCalendarWidget() {
        return calendarWidget;
    }

    public void setCalendarWidget(CalendarWidget calendarWidget) {
        this.calendarWidget = calendarWidget;
    }

    @Override
    public String toString() {
        return "Widgetbar{" +
                "caregiverStatus=" + caregiverStatus +
                ", clock=" + clock +
                ", message=" + message +
                ", picture=" + pictureWidget +
                ", volunteers=" + volunteers +
                ", weatherWidget=" + weatherWidget +
                ", calendarWidget=" + calendarWidget +
                '}';
    }
}
