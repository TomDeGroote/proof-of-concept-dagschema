package be.vivigate.dayschedule.person;

import be.vivigate.dayschedule.WebRestController;
import be.vivigate.dayschedule.helper.Time;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Status {

    private static final Logger logger = LoggerFactory.getLogger(WebRestController.class);

    private Time startTime;
    private Time endTime;
    private String from;
    private String till;
    private String description;
    private String icon;
    private String id;

    public Status(Time startTime, Time endTime, String description, String icon, String id) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.description = description;
        this.icon = icon;
        this.id = id;
    }

    public boolean isTimeInStatus(long dateMillis) {
        if (this.startTime == null) {
            this.startTime = getStartTime();
        }
        if (this.endTime == null) {
            endTime = this.getEndTime();
        }
        Calendar input = Calendar.getInstance();
        input.setTimeInMillis(dateMillis);
        if (startTime.before(dateMillis) && endTime.after(dateMillis)) {
            return true;
        }
        if (startTime.equal(dateMillis) || endTime.equal(dateMillis)) {
            return true;
        }
        return false;
    }

    public Time getStartTime() {
        try {
            if (this.startTime == null) {
                DateFormat df = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                Calendar startDate = Calendar.getInstance();
                startDate.setTime(df.parse(from));

                this.startTime = new Time(startDate.get(Calendar.HOUR_OF_DAY), startDate.get(Calendar.MINUTE),
                        startDate.get(Calendar.DAY_OF_YEAR), startDate.get(Calendar.YEAR));
            }
        } catch (ParseException e) {
            logger.error("getStartTime: ", e);
        }
        return this.startTime;
    }

    public Time getEndTime() {
        try {
            if (this.endTime == null) {
                DateFormat df = new SimpleDateFormat("HH:mm dd-MM-yyyy");
                Calendar endDate = Calendar.getInstance();
                endDate.setTime(df.parse(till));

                this.endTime = new Time(endDate.get(Calendar.HOUR_OF_DAY), endDate.get(Calendar.MINUTE),
                        endDate.get(Calendar.DAY_OF_YEAR), endDate.get(Calendar.YEAR));
            }
        } catch (ParseException e) {
            logger.error("getEndTime: ", e);
        }
        return this.endTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @JsonIgnore
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    @JsonIgnore
    public String getTill() {
        return till;
    }

    public void setTill(String till) {
        this.till = till;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
