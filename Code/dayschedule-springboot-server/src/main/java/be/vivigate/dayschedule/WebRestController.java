package be.vivigate.dayschedule;

import be.vivigate.dayschedule.activity.Activity;
import be.vivigate.dayschedule.helper.DataConverter;
import be.vivigate.dayschedule.helper.LanguageHandler;
import be.vivigate.dayschedule.helper.WeatherHandler;
import be.vivigate.dayschedule.person.Status;
import be.vivigate.dayschedule.person.StatusHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

@RestController
public class WebRestController {

    @Autowired
    private Dayschedule dayschedule;

    private final Logger logger = LoggerFactory.getLogger(WebRestController.class);


    @RequestMapping("/api/dayschedule")
    public String getDayschedule() {
        logger.info("API dayschedule");
        return DataConverter.objectToJSONString(dayschedule);
    }

    @RequestMapping("/api/layout")
    public String getLayout() {
        logger.info("API layout");
        return DataConverter.objectToJSONString(dayschedule.getLayout());
    }

    @RequestMapping(value = "/api/language", params = "id", method = RequestMethod.GET)
    public String getLanguageStrings(@RequestParam("id") String[] ids) {
        logger.info("API language with ids: " + Arrays.toString(ids));
        StringBuilder result = new StringBuilder("{");
        for (String id : ids) {
            result.append("\"").append(id).append("\": \"").append(LanguageHandler.getString(id)).append("\",");
        }
        result.deleteCharAt(result.length() - 1);
        result.append("}");
        return result.toString();
    }

    @RequestMapping(value = "/api/activities", params = "dateMillis", method = RequestMethod.GET)
    public String getActivitiesForDate(@RequestParam("dateMillis") long dateMillis) {
        logger.info("API activities with dateMillis: " + dateMillis);
        // activity list klasse
        return DataConverter.objectToJSONString(dayschedule.getPlanner().getActivitiesForDate(dateMillis));
    }

    @RequestMapping("/api/currentWeather")
    public String getCurrentWeather() {
        logger.info("API currentWeather");
        return DataConverter.objectToJSONString(WeatherHandler.getCurrentWeather());
    }

    @RequestMapping(value = "/api/activityWeather", params = "activity", method = RequestMethod.GET)
    public String getActivityWeather(@RequestParam("activity") Activity activity) {
        logger.info("API activityWeather with activity: " + activity);
        return DataConverter.objectToJSONString(WeatherHandler.getWeatherForActivity(activity));
    }

    @RequestMapping(value = "/api/statusCaregiverForTime", params = "dateMillis", method = RequestMethod.GET)
    public String getStatusCaregiverForTime(@RequestParam("dateMillis") long dateMillis) {
        logger.info("API statusCaregiverForTime: " + new Date(dateMillis));
        Status status = StatusHandler.getStatusForTime(dateMillis);
        if (status == null) {
            logger.info("returned null as status caregiver for time");
            return "null";
        }
        return DataConverter.objectToJSONString(status);
    }

    @RequestMapping("/api/screenshot")
    public String doScreenshot() {
        logger.info("API taking screenshot..");
        try {
            Runtime.getRuntime().exec("scrot screenshots/%Y%m%d_%H%M%S.png");
        } catch (IOException e) {
            logger.error("Error while taking screenshot: ", e);
        }
        return "Screenshot taken! " + new Date().toString();
    }

    @RequestMapping(value = "/api/log", params = "s", method = RequestMethod.GET)
    private void log(@RequestParam("s") String s) {
        logger.info("API frontend logging: " + s);
    }
}