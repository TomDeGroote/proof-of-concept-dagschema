package be.vivigate.dayschedule.widgetbar;

import be.vivigate.dayschedule.person.Person;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "volunteers")
public class Volunteers extends Widget {
    private List<Person> volunteers;

    public Volunteers() {
        super();
    }

    public List<Person> getVolunteers() {
        return volunteers;
    }

    public void setVolunteers(List<Person> volunteers) {
        this.volunteers = volunteers;
    }
}
