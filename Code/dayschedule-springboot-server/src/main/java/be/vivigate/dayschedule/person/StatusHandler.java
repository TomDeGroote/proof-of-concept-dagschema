package be.vivigate.dayschedule.person;

import be.vivigate.dayschedule.WebRestController;
import be.vivigate.dayschedule.activity.Activity;
import be.vivigate.dayschedule.helper.GoogleHandler;
import be.vivigate.dayschedule.helper.Time;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttachment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

@Component
@ConfigurationProperties(prefix="statushandler")
public class StatusHandler {

    private static String calendarName;
    private static Map<Time, List<Status>> statuses = new HashMap<>();
    private static Set<String> statusesIds = new HashSet<>();
    private static String defaultStatus;
    private static String defaultStatusIcon;
    private static final Logger logger = LoggerFactory.getLogger(WebRestController.class);

    public static Status getStatusForTime(long datemillis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(datemillis);
        Time keyTime = new Time(0, 0, cal.get(Calendar.DAY_OF_YEAR), cal.get(Calendar.YEAR));

        if (statuses.get(keyTime) != null) {
            for (Status status : statuses.get(keyTime)) {
                if (status.getStartTime().before(datemillis) && status.getEndTime().after(datemillis)) {
                    return status;
                }
                if (status.getStartTime().equal(datemillis) || status.getEndTime().equal(datemillis)) {
                    return status;
                }
            }
        }
        logger.warn("returning default status for date " + new Date(datemillis));
        return new Status(new Time(0, 0, 0, 0), new Time(0, 0, 0, 0),
                defaultStatus, defaultStatusIcon, "defaultStatus");
    }

    @Scheduled(fixedRate=60000)
    public void retrieveStatuses() {
        logger.info("Retrieving caregiver statuses...");
        try {
            List<Event> events = GoogleHandler.getCalendarEvents(calendarName);
            for (Event event : events) {
                if (event.getStatus().equals("cancelled")) {
                    removeStatus(event.getId());
                } else if (event.getStatus().equals("confirmed")) {
                    addStatus(eventToStatus(event));
                } else {
                    logger.warn("Event not added due to bad status!");
                }
            }
        } catch (IOException e) {
            logger.error("retrieveStatuses: ", e);
        }
    }

    private void removeStatus(String id) {
        if (statusesIds.contains(id)) {
            statusesIds.remove(id);
            for (Time key : statuses.keySet()) {
                for (Status status : statuses.get(key)) {
                    if (status.getId().equals(id)) {
                        statuses.get(key).remove(status);
                        return;
                    }
                }
            }
        }
    }

    private void addStatus(Status status) {
        if (statusesIds.contains(status.getId())) {
            removeStatus(status.getId());
        }
        Time keyTime = new Time(0, 0, status.getStartTime().getDayOfYear(), status.getStartTime().getYear());
        statuses.computeIfAbsent(keyTime, k -> new ArrayList<>());
        statuses.get(keyTime).add(status);
        statusesIds.add(status.getId());
    }

    private Status eventToStatus(Event event) {
        // Get start and end time
        Time startTime = null;
        Time endTime = null;
        if(event.getStart().getDate() != null) {
            // Whole day
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(event.getStart().getDate().getValue()));
            // TODO keep getTimeZoneShift() in mind!
            startTime = new Time(0, 0, cal.get(Calendar.DAY_OF_YEAR), cal.get(Calendar.YEAR));
            endTime =new Time(23, 59, cal.get(Calendar.DAY_OF_YEAR), cal.get(Calendar.YEAR));
        }
        if(event.getStart().getDateTime() != null) {
            startTime = new Time(event.getStart().getDateTime().getValue());
        }
        if(event.getEnd().getDateTime() != null) {
            endTime = new Time(event.getEnd().getDateTime().getValue());
        }

        // Get description (= title of activity)
        String description = event.getSummary();

        // Get icon
        String icon = "";
        if (event.getAttachments() != null) {
            for (int i = 0; i < event.getAttachments().size(); i++) {
                EventAttachment ea = event.getAttachments().get(i);
                icon = "https://docs.google.com/uc?id=" + ea.getFileId();
            }
        }

        // Get ID
        String id = event.getId();

        return new Status(startTime, endTime, description, icon, id);
    }

    public String getCalendarName() {
        return calendarName;
    }

    public void setCalendarName(String calendarName) {
        this.calendarName = calendarName;
    }

    public static String getDefaultStatus() {
        return defaultStatus;
    }

    public static void setDefaultStatus(String defaultStatus) {
        StatusHandler.defaultStatus = defaultStatus;
    }

    public static String getDefaultStatusIcon() {
        return defaultStatusIcon;
    }

    public static void setDefaultStatusIcon(String defaultStatusIcon) {
        StatusHandler.defaultStatusIcon = defaultStatusIcon;
    }
}
