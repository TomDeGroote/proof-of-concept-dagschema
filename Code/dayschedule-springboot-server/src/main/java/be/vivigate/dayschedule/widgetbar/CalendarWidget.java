package be.vivigate.dayschedule.widgetbar;

public class CalendarWidget extends Widget {

    private String calendarIcon;
    private String logo;

    public String getCalendarIcon() {
        return calendarIcon;
    }

    public void setCalendarIcon(String calendarIcon) {
        this.calendarIcon = calendarIcon;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
