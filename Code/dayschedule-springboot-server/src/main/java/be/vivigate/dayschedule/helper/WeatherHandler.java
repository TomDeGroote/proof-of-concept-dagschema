package be.vivigate.dayschedule.helper;

import be.vivigate.dayschedule.WebRestController;
import be.vivigate.dayschedule.activity.Activity;
import org.apache.http.client.HttpResponseException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class WeatherHandler {

    private static final String USER_AGENT = "Mozilla/5.0";
    private static JSONObject weatherJSON;
    private static HashMap<String, Integer> iconMapping;

    private static final String STANDARD_DESCRIPTION = "Licht bewolkt";
    private static final String STANDARD_ICON = "";

    private static final Logger logger = LoggerFactory.getLogger(WebRestController.class);

    public static Weather getCurrentWeather() {
        try {
            String icon = getWeatherIcon();
            String description = getWeatherDescription();
            return new Weather(icon, description);
        } catch (JSONException e) {
            logger.error("getCurrentWeather: ", e);
        }
        return null;
    }

    public static Weather getWeatherForActivity(Activity activity) {
        try {
            String icon = getWeatherIcon(activity.getStartTime(), activity.getEndTime());
            String description = getWeatherDescription(activity.getStartTime(), activity.getEndTime());
            return new Weather(icon, description);
        } catch (JSONException e) {
            logger.error("getWeatherForActivity: ", e);
        }
        return null;
    }

    private static Map<String, Integer> getIconMapping() {
        if (iconMapping == null) {
            iconMapping = new HashMap<>();
            iconMapping.put("snow", 5);
            iconMapping.put("rain", 4);
            iconMapping.put("cloudy", 3);
            iconMapping.put("wind", 3);
            iconMapping.put("fog", 3);
            iconMapping.put("partly-cloudy-day", 2);
            iconMapping.put("partly-cloudy-night", 2);
            iconMapping.put("clear-day", 1);
            iconMapping.put("clear-night", 1);
        }
        return iconMapping;
    }

    private static String getWeatherIcon() throws JSONException {
        String icon = weatherJSON.getJSONObject("hourly").getString("icon");
        return getIconByNumber(getIconMapping().get(icon));
    }

    private static String getIconByNumber(int nr) {
        switch (nr) {
            case 5:
                return "images/weather/snowing.png";
            case 4:
                return "images/weather/drizzle.png";
            case 3:
                return "images/weather/dark_clouds.png";
            case 2:
                return "images/weather/cloudy.png";
            case 1:
                return "images/weather/sunny.png";
            default:
                return "images/weather/cloudy.png";
        }
    }

    private static String getWeatherDescription() throws JSONException {
        return weatherJSON.getJSONObject("hourly").getString("summary");
    }

    private static String getWeatherDescription(Time startTime, Time endTime) throws JSONException {
        Date start = startTime.getDate();
        Date end = endTime.getDate();
        if (weatherJSON == null) {
            return STANDARD_DESCRIPTION;
        }
        JSONArray data =  weatherJSON.getJSONObject("hourly").getJSONArray("data");
        int mostSeriousWeather = 0;
        String description = STANDARD_DESCRIPTION;
        for (int i = 0; i < data.length(); i++) {
            JSONObject hourData = data.getJSONObject(i);
            Date hour = new Date(hourData.getLong("time") * 1000);
            if ((hour.after(start) || hour.equals(start)) && (hour.before(end) || hour.equals(end))) {
                Integer weatherSeriousness = getIconMapping().get(hourData.getString("icon"));
                if (weatherSeriousness != null && mostSeriousWeather < weatherSeriousness) {
                    mostSeriousWeather = weatherSeriousness;
                    description = hourData.getString("summary");
                }
            }
        }
        return description;
    }

    private static String getWeatherIcon(Time startTime, Time endTime) throws JSONException {
        Date start = startTime.getDate();
        Date end = endTime.getDate();
        if (weatherJSON == null) {
            return STANDARD_ICON;
        }
        JSONArray data =  weatherJSON.getJSONObject("hourly").getJSONArray("data");
        int mostSeriousWeather = 0;
        String icon = STANDARD_ICON;
        for (int i = 0; i < data.length(); i++) {
            JSONObject hourData = data.getJSONObject(i);
            Date hour = new Date(hourData.getLong("time") * 1000);
            if ((hour.after(start) || hour.equals(start)) && (hour.before(end) || hour.equals(end))) {
                Integer weatherSeriousness = getIconMapping().get(hourData.getString("icon"));
                if (weatherSeriousness != null && mostSeriousWeather < weatherSeriousness) {
                    mostSeriousWeather = weatherSeriousness;
                    icon = hourData.getString("icon");
                }
            }
        }
        if (icon.isEmpty()) {
            return getIconByNumber(0);
        }
        return getIconByNumber(getIconMapping().get(icon));
    }

    // HTTP GET request
    // source: https://www.mkyong.com/java/how-to-send-http-request-getpost-in-java/
    @Scheduled(fixedRate=600000)
    public static void updateWeather() {
        try {
            logger.info("updateWeather - Weather widget being updated...");
            String language = LanguageHandler.getLanguageString().toLowerCase();
            String url = "https://api.darksky.net/forecast/aed1638a234baf6d91a8b709d4d55fc7/50.8796,4.7009?lang="
                    + language + "&units=si";

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            //add request header
            con.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = con.getResponseCode();
            if (responseCode == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                logger.info("updateWeather -> JSON received!");
                weatherJSON = DataConverter.stringToJSONObject(response.toString());
            } else {
                logger.error("updateWeather -> Response code not 200 but " + responseCode);
            }
        } catch (IOException e) {
            logger.error("updateWeather - Error while getting weather: ", e);
        }
    }
}
