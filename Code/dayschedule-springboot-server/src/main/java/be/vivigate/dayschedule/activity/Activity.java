package be.vivigate.dayschedule.activity;

import be.vivigate.dayschedule.helper.Time;
import be.vivigate.dayschedule.helper.Weather;
import be.vivigate.dayschedule.helper.WeatherHandler;
import be.vivigate.dayschedule.person.Person;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.*;

public class Activity implements Comparable<Activity> {

    private String icon;
    private String image;
    private String title;
    private String description;
    private List<Person> attendees;
    private ActivityKind activityKind;
    private String calendarName;
    private String color;
    private String uniqueID;
    private Time startTime;
    private Time endTime;
    private List<String> recurrence;

    public Activity() {
        this.attendees = new ArrayList<>();
    }

    public Activity(String icon, String image, String title, String description, List<Person> attendees,
                    ActivityKind activityKind, String calendarName, String color,
                    String uniqueID, Time startTime, Time endTime, List<String> recurrence) {
        this.icon = icon;
        this.image = image;
        this.title = title;
        this.description = description;
        this.attendees = attendees;
        this.activityKind = activityKind;
        this.calendarName = calendarName;
        this.color = color;
        this.uniqueID = uniqueID;
        this.startTime = startTime;
        this.endTime = endTime;
        this.recurrence = recurrence;
    }

    public void addAttendee(Person person) {
        this.attendees.add(person);
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Person> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Person> attendees) {
        this.attendees = attendees;
    }

    public ActivityKind getActivityKind() {
        return activityKind;
    }

    public void setActivityKind(ActivityKind activityKind) {
        this.activityKind = activityKind;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCalendarName() {
        return calendarName;
    }

    public void setCalendarName(String calendarName) {
        this.calendarName = calendarName;
    }

    public boolean isPlanned() {
        if (activityKind == null) {
            return false;
        }
        switch (activityKind) {
            case PLANNED:
                return true;
            case SUGGESTED:
                return false;
            default:
                return false;
        }
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isRecurrent() {
        if (this.getRecurrence() != null) {
            return true;
        }
        return false;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public Weather getWeather() {
        return WeatherHandler.getWeatherForActivity(this);
    }

    @JsonIgnore
    public List<String> getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(List<String> recurrence) {
        this.recurrence = recurrence;
    }

    public boolean occursOnDate(int year, int dayOfYear) {
        if (this.isRecurrent()) {
            // TODO currently only supports weekly and then hardchecks the days
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_YEAR, dayOfYear);
            cal.set(Calendar.YEAR, year);
            int dayCode = cal.get(Calendar.DAY_OF_WEEK) - 1;
            List<String> dayCodes = Arrays.asList("SU", "MO", "TU", "WE", "TH", "FR", "SA");
            String daysOfRecurrence = this.getRecurrence().get(0)
                    .replace("RRULE", "")
                    .replace("FREQ", "")
                    .replace("WEEKLY", "")
                    .replace("BYDAY", "");

            if (daysOfRecurrence != null && daysOfRecurrence.contains(dayCodes.get(dayCode))) {
                return true;
            }
            return false;
        } else {
            if (this.getStartTime().getDayOfYear() == dayOfYear
                    && this.getStartTime().getYear() == year) {
                return true;
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Activity activity = (Activity) o;
        return uniqueID.equals(activity.uniqueID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uniqueID);
    }


    @Override
    public int compareTo(Activity other) {
        // Check difference in starttime
        int hourDiff = this.getStartTime().getHour() - other.getStartTime().getHour();
        if (hourDiff != 0) {
            return hourDiff;
        }
        int minuteDiff = this.getStartTime().getMinute() - other.getStartTime().getMinute();
        if (minuteDiff != 0) {
            return hourDiff;
        }

        // If no difference, check difference in end time
        int hourDiffEnd = this.getEndTime().getHour() - other.getEndTime().getHour();
        if (hourDiffEnd != 0) {
            return hourDiffEnd;
        }

        int minuteDiffEnd = this.getEndTime().getMinute() - other.getEndTime().getMinute();
        if (minuteDiffEnd != 0) {
            return minuteDiffEnd;
        }

        return 0;
    }

    public Activity duplicateForDate(int year, int dayOfYear) {
        Time startTime = null;
        if (this.startTime != null) {
            startTime = new Time(this.startTime.getHour(), this.startTime.getMinute(), dayOfYear, year);
        }
        Time endTime = null;
        if (this.endTime != null) {
            endTime = new Time(this.endTime.getHour(), this.endTime.getMinute(), dayOfYear, year);
        }
        Activity newActivity = new Activity(this.icon, this.image, this.title, this.description, this.attendees,
                this.activityKind, this.calendarName, this.color, this.uniqueID, startTime, endTime,
                this.recurrence);
        return newActivity;
    }

    public Time getStartDate() {
        Time t = this.getStartTime();
        return new Time(0, 0, t.getDayOfYear(), t.getYear());
    }

}
