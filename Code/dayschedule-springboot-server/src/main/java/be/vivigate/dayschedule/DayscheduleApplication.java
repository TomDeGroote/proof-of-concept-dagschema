package be.vivigate.dayschedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

// FIXME bugfix klokjes in wegweergave
// TODO klikken op een dubbele activiteit, welke geef je weer?
// FIXME als er geen activiteiten zijn dan werkt het niet
// TODO afbeeldingen inladen via server ipv via de frontend
// TODO afbeeldingen in height of width laten matchen afhankelijk van de grootste size
@SpringBootApplication
@EnableScheduling
public class DayscheduleApplication {


    public static String APPLICATION_NAME = "ViviGate Dayschedule";

	public static void main(String[] args) {
		SpringApplication.run(DayscheduleApplication.class, args);
	}

}
