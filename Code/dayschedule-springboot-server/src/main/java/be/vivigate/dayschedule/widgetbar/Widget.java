package be.vivigate.dayschedule.widgetbar;

public abstract class Widget {
    private boolean visible;

    public Widget() {
        this.visible = false;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
