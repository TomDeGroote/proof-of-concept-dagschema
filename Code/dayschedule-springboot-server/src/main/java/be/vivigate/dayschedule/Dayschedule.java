package be.vivigate.dayschedule;

import be.vivigate.dayschedule.activity.Planner;
import be.vivigate.dayschedule.layout.Layout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="dayschedule")
public class Dayschedule {

    private Layout layout;

    @Autowired
    private Planner planner;

    public Layout getLayout() {
        return layout;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    public Planner getPlanner() {
        return planner;
    }

    public void setPlanner(Planner planner) {
        this.planner = planner;
    }
}
