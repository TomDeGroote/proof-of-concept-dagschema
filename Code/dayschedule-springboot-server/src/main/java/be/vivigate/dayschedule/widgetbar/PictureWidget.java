package be.vivigate.dayschedule.widgetbar;

import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.util.List;

@Component
public class PictureWidget extends Widget {

    private List<Picture> pictures;
    private int maxNrOfPictures;

    public PictureWidget() {
        super();
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }

    public int getMaxNrOfPictures() {
        return Math.min(maxNrOfPictures, pictures.size());
    }

    public void setMaxNrOfPictures(int maxNrOfPictures) {
        this.maxNrOfPictures = maxNrOfPictures;
    }
}
