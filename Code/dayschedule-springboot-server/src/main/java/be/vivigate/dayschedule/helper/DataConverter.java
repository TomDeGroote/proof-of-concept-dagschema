package be.vivigate.dayschedule.helper;

import be.vivigate.dayschedule.WebRestController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataConverter {

    private static final Logger logger = LoggerFactory.getLogger(WebRestController.class);

    public static JSONObject stringToJSONObject(String jsonString) {
        try {
            return new JSONObject(jsonString);
        } catch (JSONException e) {
            logger.error("stringToJSONObject - error converting string to JSON: ", e);
        }
        return null;
    }

    public static String objectToJSONString(Object obj) {
        ObjectMapper mapper = new ObjectMapper();
        JSONObject jsonObject = null;
        try {
            String jsonString = mapper.writeValueAsString(obj);
            jsonObject = new JSONObject(jsonString);
        } catch (JsonProcessingException | JSONException e) {
            logger.error("objectToJSONString - error converting JSON to string: ", e);
        }
        return jsonObject.toString();
    }
}

