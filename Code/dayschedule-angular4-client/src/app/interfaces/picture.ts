export interface Picture {
  image: string;
  description: string;
}
