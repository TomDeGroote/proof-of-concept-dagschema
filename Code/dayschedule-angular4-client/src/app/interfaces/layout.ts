import {WidgetBar} from './widgetbar';

export interface Layout {
  layoutElements: {};
  widgetbar: WidgetBar;
  layoutName: string;
}
