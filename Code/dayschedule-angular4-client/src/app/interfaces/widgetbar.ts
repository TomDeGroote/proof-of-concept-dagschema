import {Person} from './person';
import {Picture} from './picture';

export interface WidgetBar {
  caregiverStatus: CaregiverStatusWidget;
  clock: any;
  message: any;
  pictureWidget: PictureWidget;
  volunteers: Volunteers;
  calendarWidget: CalendarWidget;
}

export interface Widget {
  visible: boolean;
}

export interface CalendarWidget extends Widget {
  calendarIcon: string;
  logo: string;
}

export interface Volunteers extends Widget {
  volunteers: [Person];
}

export interface CaregiverStatusWidget extends Widget {
  caregiver: Person;
  icon: string;
}

export interface PictureWidget extends Widget {
  maxNrOfPictures: number;
  pictures: [Picture];
}
