import {WidgetBar} from './widgetbar';

export interface DaySchedule {
  widgetbar: WidgetBar;
  layoutName: string;
}
