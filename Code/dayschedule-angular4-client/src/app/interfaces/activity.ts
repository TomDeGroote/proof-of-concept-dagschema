import {Person} from './person';
import {Time} from './time';
import {Weather} from './weather';

export interface Activity {
  icon: string;
  image: string;
  title: string;
  description: string;
  attendees: [Person];
  activityKind: string;
  calendarName: string;
  overlapping: number;
  color: string;
  overlappingActivity: Activity;
  uniqueID: string;
  startTime: Time;
  endTime: Time;
  weather: Weather;
}
