export interface Time {
  hour: number;
  minute: number;
  dayOfYear: number;
  year: number;
  date: Date;
  millis: number;
}
