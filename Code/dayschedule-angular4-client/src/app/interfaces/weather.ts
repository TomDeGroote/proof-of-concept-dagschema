export interface Weather {
  icon: string;
  description: string;
}
