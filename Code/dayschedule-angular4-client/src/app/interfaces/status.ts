import {Time} from './time';

export interface Status {
  startTime: Time;
  endTime: Time;
  description: string;
  icon: string;
}
