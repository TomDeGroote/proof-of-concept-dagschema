import {Status} from './status';

export interface Person {
  picture: string;
  firstName: string;
  lastName: string;
  description: string;
  status: [Status];
  currentStatus: Status;
}
