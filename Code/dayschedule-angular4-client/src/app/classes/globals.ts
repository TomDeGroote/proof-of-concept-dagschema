import {Injectable} from '@angular/core';

@Injectable()
export class Globals {
  hostName: string;

  constructor() {
    this.hostName = 'http://localhost:8080/';
  }
}
