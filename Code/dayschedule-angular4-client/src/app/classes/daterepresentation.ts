export class DateRepresentation {
  day: string;
  date: number;
  month: string;
  year: number;

  constructor(day: string, date: number, month: string, year: number) {
    this.day = day;
    this.date = date;
    this.month = month;
    this.year = year;
  }
}
