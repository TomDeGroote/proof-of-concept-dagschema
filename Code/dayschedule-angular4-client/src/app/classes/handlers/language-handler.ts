import {TimeHandler} from './time-handler';
import {Injectable} from '@angular/core';
import {DateRepresentation} from '../daterepresentation';

@Injectable()
export class LanguageHandler {

  languageTerms: any;
  currentDate: DateRepresentation;

  constructor(public timehandler: TimeHandler) {
  }

  getTerm(term: string) {
    return this.languageTerms[term];
  }

  getMonthCode() {
    return ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'][this.timehandler.getDate().getMonth()];
  }
}
