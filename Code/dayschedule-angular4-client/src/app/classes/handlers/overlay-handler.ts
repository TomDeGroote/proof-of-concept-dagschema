import {Activity} from '../../interfaces/activity';
import {ActivityHandler} from './activity-handler';
import {Injectable} from '@angular/core';
import {HttpClientHandler} from './http-client-handler';

@Injectable()
export class OverlayHandler {

  overlayDisplay: string;
  overlayApp: string;

  constructor(public activityHandler: ActivityHandler,
              public httpclienthandler: HttpClientHandler) {
    this.overlayApp = 'none';
    this.overlayDisplay = 'none';
  }

  toDisplay(value: string) {
    if (this.overlayApp === value) {
      return 'block';
    }
    return 'none';
  }

  showOverlay(value: string, clickedActivity: Activity) {
    this.httpclienthandler.log('CLICK -> showOverlay: ' + value);
    this.overlayDisplay = 'block';
    this.overlayApp = value;
    this.activityHandler.clickedActivity = clickedActivity;
  }
}
