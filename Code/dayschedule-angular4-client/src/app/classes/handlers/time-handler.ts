import {Injectable} from '@angular/core';

@Injectable()
export class TimeHandler {
  speed: number;
  actualStartTime: number;
  setStartTime: number;
  timerRefreshRate: number;
  timerRefreshRatePictures: number;

  lookBackTime: number;
  lookForwardTime: number;
  weekDays: [Date];

  constructor() {
    const speed: number = 0;
    this.setStartTime = new Date().getTime();
    this.actualStartTime = new Date().getTime();
    this.setStartTime = 1518436800000 - (0) * 60 * 60 * 1000 + 116 * 24 * 60 * 60 * 1000;
    this.timerRefreshRatePictures = 1000 * 30 * 60; // TODO increase to 30 minutes for refresh
    this.lookBackTime = 0; // 0.1 * 60 * 60 * 1000; // Look back for 30 min
    this.lookForwardTime = 9 * 60 * 60 * 1000; // Look forward for 9 hours.
    if (speed === 0) {
      this.timerRefreshRate = 1000 * 30;
      this.speed = 1;
    } else if (speed === 1) {
      this.timerRefreshRate = 1000 * 5;
      this.speed = 10;
    } else {
      this.timerRefreshRate = 100;
      this.speed = 4000;
    }
  }

  getDate() {
    return new Date(this.setStartTime + (new Date().getTime() - this.actualStartTime) * this.speed);
  }

  getWeekDays() {
    const currentDate = this.getDate();
    const day = currentDate.getDay();
    const millisConverter = 24 * 60 * 60 * 1000;
    currentDate.setHours(0, 0, 0, 0);
    if (day === 0) {
      this.weekDays = [
        new Date(currentDate.getTime() - 6 * millisConverter),
        new Date(currentDate.getTime() - 5 * millisConverter),
        new Date(currentDate.getTime() - 4 * millisConverter),
        new Date(currentDate.getTime() - 3 * millisConverter),
        new Date(currentDate.getTime() - 2 * millisConverter),
        new Date(currentDate.getTime() - millisConverter),
        new Date(currentDate.getTime()),
      ];
    } else {
      this.weekDays = [
        new Date(currentDate.getTime() + (1 - day) * millisConverter),
        new Date(currentDate.getTime() + (2 - day) * millisConverter),
        new Date(currentDate.getTime() + (3 - day) * millisConverter),
        new Date(currentDate.getTime() + (4 - day) * millisConverter),
        new Date(currentDate.getTime() + (5 - day) * millisConverter),
        new Date(currentDate.getTime() + (6 - day) * millisConverter),
        new Date(currentDate.getTime() + (7 - day) * millisConverter)
      ];
    }
  }
}
