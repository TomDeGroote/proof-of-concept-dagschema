import {Layout} from '../../interfaces/layout';
import {Injectable} from '@angular/core';
import {TimeHandler} from './time-handler';
import {HttpClientHandler} from './http-client-handler';
import {Status} from '../../interfaces/status';

@Injectable()
export class LayoutHandler {

  layout: Layout;
  pictureIndex = 0;
  pictureIndexList = undefined;
  randomIndices = undefined;
  httpclienthandler: HttpClientHandler;
  currentStatus: Status;

  constructor(public timehandler: TimeHandler) {
  }

  getLayoutElement(element: string) {
    return this.layout.layoutElements[element];
  }

  generatePictureIndexList() {
    const maxNrOfPictures = this.layout.widgetbar.pictureWidget.maxNrOfPictures;
    if (this.randomIndices === undefined || this.randomIndices.length !== this.layout.widgetbar.pictureWidget.pictures.length) {
      this.randomIndices = [];
      for (let i = 0; i < this.layout.widgetbar.pictureWidget.pictures.length; i++) {
        this.randomIndices.push(i);
      }
    }
    this.shuffle(this.randomIndices);
    this.pictureIndexList = this.randomIndices.slice(0, maxNrOfPictures);
  }

  shuffle(a) {
    let j, x, i;
    for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
    }
  }

  initializePictureIndexList() {
    this.generatePictureIndexList();
    setInterval(() =>  {
      this.generatePictureIndexList();
    }, this.timehandler.timerRefreshRatePictures);
  }

  getPictureIndex() {
    return this.pictureIndexList[this.pictureIndex];
  }

  getNextPicture() {
    this.httpclienthandler.log('CLICK -> Next picture');
    this.pictureIndex++;
    if (this.pictureIndex >= this.pictureIndexList.length) {
      this.pictureIndex = 0;
    }
  }

  getPreviousPicture() {
    this.httpclienthandler.log('CLICK -> Previous picture');
    this.pictureIndex--;
    if (this.pictureIndex < 0) {
      this.pictureIndex = this.pictureIndexList.length - 1;
    }
  }
}
