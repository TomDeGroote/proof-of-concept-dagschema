import {HttpClient} from '@angular/common/http';
import {TimeHandler} from './time-handler';
import {Layout} from '../../interfaces/layout';
import {LayoutHandler} from './layout-handler';
import {LanguageHandler} from './language-handler';
import {ActivityHandler} from './activity-handler';
import {Injectable} from '@angular/core';
import {DateRepresentation} from '../daterepresentation';
import {Activity} from '../../interfaces/activity';
import {Globals} from '../globals';
import {Status} from '../../interfaces/status';

@Injectable()
export class HttpClientHandler {

  constructor(public http: HttpClient,
              public timehandler: TimeHandler,
              public layouthandler: LayoutHandler,
              public languagehandler: LanguageHandler,
              public activityhandler: ActivityHandler,
              public globals: Globals) {

    this.getLayoutInitial();
    this.getLanguageTerms();
    this.getDateToDisplay();
    this.loadActivities();
    this.getCurrentStatusCaregiver();
    this.layouthandler.httpclienthandler = this;
    this.activityhandler.httpclienthandler = this;
    setInterval(() =>  {
      this.getLayout();
      this.getCurrentStatusCaregiver();
      this.getLanguageTerms();
      this.getDateToDisplay();
      this.loadActivities();
    }, timehandler.timerRefreshRate);
  }

  getLayoutInitial() {
    const httpSubscription = this.http.get<Layout>('/api/layout').subscribe(layout =>  {
      // Read the result field from the JSON response.
      httpSubscription.unsubscribe();
      this.layouthandler.layout = layout;
      this.layouthandler.initializePictureIndexList();
      this.log('getLayoutInitial -> Layout retrieved!');
    });
  }

  getLayout() {
    const httpSubscription = this.http.get<Layout>('/api/layout').subscribe(layout =>  {
      // Read the result field from the JSON response.
      httpSubscription.unsubscribe();
      this.layouthandler.layout = layout;
      this.log('getLayout -> Layout retrieved!');
    });
  }

  getCurrentStatusCaregiver() {
    const httpSubscription = this.http.get<Status>('/api/statusCaregiverForTime?dateMillis=' + this.timehandler.getDate().getTime())
      .subscribe(status =>  {
      // Read the result field from the JSON response.
      console.log('Status retrieved: ', status);
      httpSubscription.unsubscribe();
      this.layouthandler.currentStatus = status;
      this.log('getCurrentStatusCaregiver -> Status caregiver retrieved!');
    });
  }

  getLanguageTerms() {
    const toRetrieve = [
      'moShort',
      'tuShort',
      'wedShort',
      'thShort',
      'frShort',
      'saShort',
      'suShort',
      'calendar',
      'thehouse',
      'host',
      'eveningactivity',
      'closed',
      'open',
      'hourShort',
      'back',
      'now',
      'jan',
      'feb',
      'mar',
      'apr',
      'may',
      'jun',
      'jul',
      'aug',
      'sep',
      'nov',
      'oct',
      'dec',
      'from',
      'till'];

    const httpSubscription = this.http.get('/api/language?id=' + toRetrieve).subscribe(res => { // Success
        httpSubscription.unsubscribe();
        const newLanguageTerms = {};
        for (let i = 0; i < toRetrieve.length; i++) {
          newLanguageTerms[toRetrieve[i]] = res[toRetrieve[i]];
        }
        this.languagehandler.languageTerms = newLanguageTerms;
        this.log('getLanguageTerms -> Language terms received!');
      },
      msg => { // Error
        console.log(msg);
      }
    );
  }

  getDateToDisplay() {
    const date = this.timehandler.getDate();
    const dayCode = ['su', 'mo', 'tu', 'wed', 'th', 'fr', 'sa'][date.getDay()];
    const monthCode = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'nov', 'oct', 'dec'][date.getMonth()];

    const languageSubscription = this.http.get('/api/language?id=' + [dayCode, monthCode]).subscribe(
      (res) => { // Success
        this.languagehandler.currentDate = new DateRepresentation(res[dayCode].charAt(0).toUpperCase() + res[dayCode].slice(1),
          date.getDate(),
          res[monthCode],
          date.getFullYear());
        languageSubscription.unsubscribe();
        this.log('getDateToDisplay -> Date to display retrieved!');
      },
      msg => { // Error
        console.log(msg);
      }
    );
  }

  loadActivities() {
    this.timehandler.getWeekDays();
    const weekDays = this.timehandler.weekDays;
    for (let weekDayIndex = 0; weekDayIndex < weekDays.length; weekDayIndex++) {
      const activitySubscription = this.http.get('/api/activities?dateMillis=' + weekDays[weekDayIndex].getTime()).subscribe(
        (res) => { // Success
          activitySubscription.unsubscribe();
          const retrievedDayActivities = <[Activity]>res['activities'];
          this.log('loadActivities -> activities retrieved!');

          this.activityhandler.removeForDay(weekDayIndex);

          for (let j = 0; j < retrievedDayActivities.length; j++) {
            this.activityhandler.addActivity(retrievedDayActivities[j], weekDayIndex);
          }
        },
        msg => { // Error
          console.log(msg);
        }
      );
    }
  }

  log(s: string) {
    const logSubscription = this.http.get('/api/log?s=' + s).subscribe(
      (res) => { // Success
        logSubscription.unsubscribe();
      },
      msg => { // Error
        console.log(msg);
      }
    );
  }
}
