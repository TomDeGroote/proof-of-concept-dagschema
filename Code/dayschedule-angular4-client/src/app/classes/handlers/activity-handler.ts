import {Activity} from '../../interfaces/activity';
import {TimeHandler} from './time-handler';
import {Injectable} from '@angular/core';
import {LanguageHandler} from './language-handler';
import {Globals} from '../globals';
import {HttpClientHandler} from './http-client-handler';

@Injectable()
export class ActivityHandler {

  clickedActivity: Activity;
  exceptionActivities = ['Vanaf 9u: Verwelkoming met koffie', 'We zwaaien onze bezoekers uit (tot 5u)'];
  calendarName: string;

  activities: [[Activity]];
  httpclienthandler: HttpClientHandler;


  constructor(public timehandler: TimeHandler,
              public languagehandler: LanguageHandler,
              public globals: Globals) {
    this.activities = [<[Activity]>[], <[Activity]>[], <[Activity]>[],
      <[Activity]>[], <[Activity]>[], <[Activity]>[], <[Activity]>[]];
    this.calendarName = 'Proof-Of-Concept';
  }

  getActivityIcon(activity: Activity) {
    if (activity.icon === null) {
      return '';
    } else if (activity.icon.startsWith('http')) {
      return activity.icon;
    } else {
      return this.globals.hostName + activity.icon;
    }
  }

  getActivityImage(activity: Activity) {
    if (activity.image === null) {
      return null;
    } else if (activity.image.startsWith('http')) {
      return activity.image;
    } else {
      return this.globals.hostName + activity.image;
    }
  }

  getActivitiesToDisplay() {
    const activities = [];
    const currentTime = this.timehandler.getDate().getTime();
    const minTime = currentTime - this.timehandler.lookBackTime;
    const maxTime = currentTime + this.timehandler.lookForwardTime;
    const calendarName = this.calendarName;
    this.getActivitiesForDate(this.timehandler.getDate()).forEach(function (activity) {
      if (activity.endTime.millis > minTime
        && activity.startTime.millis <= maxTime
        && activity.calendarName === calendarName) {
        if (!(activity.overlapping === 2 && activity.overlappingActivity.calendarName === calendarName)) {
          activities.push(activity);
        }
      }
    });
    return activities;
  }

  getActivitiesForDate(date: Date) {
    if (date.getDay() === 0) {
      return this.activities[6];
    }
    return this.activities[date.getDay() - 1];
  }

  removeForDay(weekDayIndex: number) {
    this.activities[weekDayIndex] = <[Activity]>[];
  }

  addActivity(activity: Activity, weekDayIndex: number) {
    if (activity.activityKind === 'SUGGESTED') {
      return;
    }
    // In day activity
    const compareToActivities = this.activities[weekDayIndex];
    activity.overlapping = 0;
    for (let i = 0; i < compareToActivities.length; i++) {
      const compareToActivity = compareToActivities[i];

      // Check if activity already exists in view, if so return
      if (compareToActivity.uniqueID === activity.uniqueID) {
        compareToActivities[i] = activity;
        return;
      }

      // Check if activities overlap
      this.overlap(compareToActivity, activity);
    }
    this.activities[weekDayIndex].push(activity);
  }

  // TODO only supports two overlapping activities
  overlap(act1: Activity, act2: Activity) {
    if (act1.startTime.millis >= act2.startTime.millis && act1.startTime.millis < act2.endTime.millis) {
      act1.overlappingActivity = act2;
      act2.overlappingActivity = act1;
      if (act1.overlapping === 1 || act2.overlapping === 2) {
        act1.overlapping = 1;
        act2.overlapping = 2;
      } else if (act1.overlapping === 2 || act2.overlapping === 1) {
        act1.overlapping = 2;
        act2.overlapping = 1;
      } else {
        act1.overlapping = 2;
        act2.overlapping = 1;
      }
    } else if (act2.startTime.millis >= act1.startTime.millis
      && act2.startTime.millis < act1.endTime.millis) {
      act1.overlappingActivity = act2;
      act2.overlappingActivity = act1;
      if (act1.overlapping === 1 || act2.overlapping === 2) {
        act1.overlapping = 1;
        act2.overlapping = 2;
      } else if (act1.overlapping === 2 || act2.overlapping === 1) {
        act1.overlapping = 2;
        act2.overlapping = 1;
      } else {
        act1.overlapping = 1;
        act2.overlapping = 2;
      }
    }
  }

  getOpenOrClosedColors(activity: string) {
    if (activity === 'open') {
      return '#3caa36';
    } else {
      return '#e73529';
    }
  }

  checkOpenOrClosed(activity: Activity) {
    if (activity.title.includes('open')) {
      return this.languagehandler.getTerm('open');
    }
    return this.languagehandler.getTerm('closed');
  }

  getTimeStamp(activity: Activity) {
    if (this.exceptionActivities.indexOf(activity.title) >= 0) {
      return '';
    }
    let startHour = activity.startTime.hour;
    if (startHour > 12) {
      startHour %= 12;
    }
    const startMinutes = activity.startTime.minute;
    let startRepresentation = '';
    if (startMinutes === 0) {
      startRepresentation = startHour + this.languagehandler.getTerm('hourShort');
    } else {
      startRepresentation = startHour + this.languagehandler.getTerm('hourShort') + startMinutes;
    }

    let endHour = activity.endTime.hour;
    if (endHour > 12) {
      endHour %= 12;
    }
    const endMinutes = activity.endTime.minute;
    let endRepresentation = '';
    if (endMinutes === 0) {
      endRepresentation = endHour + this.languagehandler.getTerm('hourShort');
    } else {
      endRepresentation = endHour + this.languagehandler.getTerm('hourShort') + endMinutes;
    }

    return startRepresentation + ' - ' + endRepresentation;
  }
}
