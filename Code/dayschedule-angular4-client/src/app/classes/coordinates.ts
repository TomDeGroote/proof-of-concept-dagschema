export class Coordinates {
  cx: number;
  cy: number;
  r: number;
  lx: number;
  ly: number;
  ax: number;
  ay: number;

  constructor(cx: number, cy: number, r: number, lx: number,  ly: number, ax: number, ay: number) {
    this.cx = cx;
    this.cy = cy;
    this.r = r;
    this.lx = lx;
    this.ly = ly;
    this.ax = ax;
    this.ay = ay;
  }
}
