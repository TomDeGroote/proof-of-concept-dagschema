import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../../app.component';
import {Globals} from '../../classes/globals';
import {LayoutHandler} from '../../classes/handlers/layout-handler';
import {LanguageHandler} from '../../classes/handlers/language-handler';

@Component({
  selector: 'app-caregiver-widget',
  templateUrl: './caregiver-widget.component.html',
  styleUrls: ['./caregiver-widget.component.css'],
  providers: [AppComponent]
})
export class CaregiverWidgetComponent implements OnInit {

  constructor(private globals: Globals,
              private layouthandler: LayoutHandler) { }

  ngOnInit() {
  }

}
