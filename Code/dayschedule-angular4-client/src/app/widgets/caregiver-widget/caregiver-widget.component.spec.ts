import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverWidgetComponent } from './caregiver-widget.component';

describe('CaregiverWidgetComponent', () => {
  let component: CaregiverWidgetComponent;
  let fixture: ComponentFixture<CaregiverWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaregiverWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
