import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicturesWidgetComponent } from './pictures-widget.component';

describe('PicturesWidgetComponent', () => {
  let component: PicturesWidgetComponent;
  let fixture: ComponentFixture<PicturesWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicturesWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicturesWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
