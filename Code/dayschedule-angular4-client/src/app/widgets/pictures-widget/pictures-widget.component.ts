import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../../app.component';
import {OverlayHandler} from '../../classes/handlers/overlay-handler';
import {Globals} from '../../classes/globals';
import {LayoutHandler} from '../../classes/handlers/layout-handler';

@Component({
  selector: 'app-pictures-widget',
  templateUrl: './pictures-widget.component.html',
  styleUrls: ['./pictures-widget.component.css'],
  providers: [AppComponent]
})
export class PicturesWidgetComponent implements OnInit {

  constructor(public overlayhandler: OverlayHandler,
              public globals: Globals,
              public layouthandler: LayoutHandler) { }

  ngOnInit() {
  }

}
