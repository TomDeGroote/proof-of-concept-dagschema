import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VolunteerWidgetComponent } from './volunteer-widget.component';

describe('VolunteerWidgetComponent', () => {
  let component: VolunteerWidgetComponent;
  let fixture: ComponentFixture<VolunteerWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VolunteerWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VolunteerWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
