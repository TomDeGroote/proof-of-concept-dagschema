import {Component, Input, OnInit} from '@angular/core';
import {LayoutHandler} from '../../classes/handlers/layout-handler';

@Component({
  selector: 'app-volunteer-widget',
  templateUrl: './volunteer-widget.component.html',
  styleUrls: ['./volunteer-widget.component.css']
})
export class VolunteerWidgetComponent implements OnInit {

  @Input() nrOfVolunteers: number;

  constructor(private layouthandler: LayoutHandler) { }

  ngOnInit() {
    if (this.nrOfVolunteers === undefined) {
      this.nrOfVolunteers = this.layouthandler.layout.widgetbar.volunteers.volunteers.length;
    }
    if (this.nrOfVolunteers > this.layouthandler.layout.widgetbar.volunteers.volunteers.length) {
      this.nrOfVolunteers = this.layouthandler.layout.widgetbar.volunteers.volunteers.length;
    }
  }

}
