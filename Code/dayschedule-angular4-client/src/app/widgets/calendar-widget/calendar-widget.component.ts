import {Component, Input, OnInit} from '@angular/core';
import {DateRepresentation} from '../../classes/daterepresentation';
import {LanguageHandler} from '../../classes/handlers/language-handler';
import {OverlayHandler} from '../../classes/handlers/overlay-handler';
import {Globals} from '../../classes/globals';
import {LayoutHandler} from '../../classes/handlers/layout-handler';
import {Layout} from '../../interfaces/layout';
import {TimeHandler} from '../../classes/handlers/time-handler';

@Component({
  selector: 'app-calendar-widget',
  templateUrl: './calendar-widget.component.html',
  styleUrls: ['./calendar-widget.component.css']
})
export class CalendarWidgetComponent implements OnInit {

  @Input() layout: Layout;
  @Input() millis: number;

  constructor(private languagehandler: LanguageHandler,
              private overlayhandler: OverlayHandler,
              private globals: Globals,
              private layouthandler: LayoutHandler,
              private timehandler: TimeHandler) {
  }

  ngOnInit() {}

  getSmallTransform() {
    let hour;
    if (this.millis === undefined) {
      hour = this.timehandler.getDate().getHours() % 12 * 60 + this.timehandler.getDate().getMinutes();
    } else {
      const date = new Date(this.millis);
      hour = date.getHours() % 12 * 60 + date.getMinutes();
    }
    const degrees = 360 / 12 / 60 * hour;
    return 'rotate(' + degrees + ', 100, 100)';
  }

  getBigTransform() {
    let minute;
    if (this.millis === undefined) {
      minute = this.timehandler.getDate().getMinutes();
    } else {
      minute = new Date(this.millis).getMinutes();
    }
    const degrees = 360 / 60 * minute;
    return 'rotate(' + degrees + ', 100, 100)';
  }
}
