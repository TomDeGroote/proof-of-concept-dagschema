import {Component, Input, OnInit} from '@angular/core';
import {Layout} from '../../interfaces/layout';
import {TimeHandler} from '../../classes/handlers/time-handler';
import {AppComponent} from '../../app.component';
import {Globals} from '../../classes/globals';
import {LayoutHandler} from '../../classes/handlers/layout-handler';

@Component({
  selector: 'app-time-widget',
  templateUrl: './time-widget.component.html',
  styleUrls: ['./time-widget.component.css'],
  providers: [AppComponent]
})
export class TimeWidgetComponent implements OnInit {

  @Input() layout: Layout;
  @Input() millis: number;

  constructor(private timehandler: TimeHandler,
              private globals: Globals,
              private layouthandler: LayoutHandler) { }

  ngOnInit() {
  }

  getSmallTransform() {
    let hour;
    if (this.millis === undefined) {
      hour = this.timehandler.getDate().getHours() % 12 * 60 + this.timehandler.getDate().getMinutes();
    } else {
      const date = new Date(this.millis);
      hour = date.getHours() % 12 * 60 + date.getMinutes();
    }
    const degrees = 360 / 12 / 60 * hour;
    return 'rotate(' + degrees + ', 100, 100)';
  }

  getBigTransform() {
    let minute;
    if (this.millis === undefined) {
      minute = this.timehandler.getDate().getMinutes();
    } else {
      minute = new Date(this.millis).getMinutes();
    }
    const degrees = 360 / 60 * minute;
    return 'rotate(' + degrees + ', 100, 100)';
  }

}
