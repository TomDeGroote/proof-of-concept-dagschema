import {Component} from '@angular/core';
import {LayoutHandler} from './classes/handlers/layout-handler';
import {HttpClientHandler} from './classes/handlers/http-client-handler';
import {TimeHandler} from './classes/handlers/time-handler';
import {ActivityHandler} from './classes/handlers/activity-handler';
import {LanguageHandler} from './classes/handlers/language-handler';
import {OverlayHandler} from './classes/handlers/overlay-handler';
import {Globals} from './classes/globals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [LayoutHandler,
              HttpClientHandler,
              TimeHandler,
              ActivityHandler,
              LanguageHandler,
              OverlayHandler,
              Globals]
})
export class AppComponent {

  constructor(public layouthandler: LayoutHandler,
              public httpclienthandler: HttpClientHandler,
              public timehandler: TimeHandler,
              public activityhandler: ActivityHandler,
              public languagehandler: LanguageHandler,
              public overlayhandler: OverlayHandler,
              public globals: Globals) {}
}
