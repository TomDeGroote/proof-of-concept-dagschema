import {Component, OnInit} from '@angular/core';
import {Activity} from '../../interfaces/activity';
import {TimeHandler} from '../../classes/handlers/time-handler';
import {AppComponent} from '../../app.component';

@Component({
  selector: 'app-walk',
  templateUrl: './walk.component.html',
  styleUrls: ['./walk.component.css'],
  providers: [AppComponent]
})
export class WalkComponent implements OnInit {
  //
  // leftGlare = 0;
  //
  // constructor(private timehandler: TimeHandler) { }
  //
  ngOnInit() {
    // this.timehandler.lookBackTime = 2 * 60 * 60 * 1000;
  }
  //
  // // 1080 - 105 = 975
  // // 670, 900
  // getActivityPolygon(activity: Activity) {
  //   // Bottom line
  //   const bottomHeight = this.getHeightForTime(activity.time.startDate);
  //   const bottomWidth = this.getWidthAtHeight(bottomHeight);
  //   const bottomLeft = (900 - bottomWidth) / 2;
  //
  //   // Top line
  //   const topHeight = this.getHeightForTime(activity.time.endDate) + 15;
  //   const topWidth = this.getWidthAtHeight(topHeight);
  //   const topLeft = (900 - topWidth) / 2;
  //
  //   return bottomLeft + ','  + bottomHeight + ' ' +
  //         (bottomLeft + bottomWidth) + ','  + bottomHeight + ' ' +
  //         (topLeft + topWidth) + ','  + topHeight + ' ' +
  //          topLeft + ','  + topHeight;
  // }
  //
  // getWidthAtHeight(height: number) {
  //   return 630 + 230 * height / 1080;
  // }
  //
  // getHeightForTime(dateMs: number) {
  //   const difference = dateMs - this.timehandler.getDate().getTime();
  //   const pxPerMs = 975 / this.timehandler.lookForwardTime;
  //   return 1080 - (105 + pxPerMs * difference);
  // }
  //
  // getWidthActivity(activity: Activity) {
  //   const endTimeHeight = this.getHeightForTime(activity.time.endDate);
  //   const w = this.getWidthAtHeight(endTimeHeight);
  //   return w + 'px';
  // }
  //
  // getHeightActivity(activity: Activity) {
  //   const startHeight = this.getHeightForTime(activity.time.startDate);
  //   const endHeight = this.getHeightForTime(activity.time.endDate);
  //   return (startHeight - endHeight - 15) + 'px';
  // }
  //
  // getBottomActivity(activity: Activity) {
  //   return 1079 - this.getHeightForTime(activity.time.startDate) + 'px';
  // }
  //
  // getBottomIcon(activity: Activity) {
  //   const startHeight = this.getHeightForTime(activity.time.startDate);
  //   const endHeight = this.getHeightForTime(activity.time.endDate);
  //   let bottom = (startHeight - endHeight - 15) / 2 - 60;
  //   if (startHeight - bottom > 960) {
  //     bottom = startHeight - 960;
  //     if ((bottom + 120) > (startHeight - endHeight - 15) - 10) {
  //       bottom = (startHeight - endHeight - 15) - 130;
  //     }
  //   }
  //   return  bottom + 'px';
  // }
  //
  // getBottomText(activity: Activity) {
  //   const element = document.getElementById(activity.uniqueID + '');
  //   let bottom = 0;
  //   if (element !== null) {
  //     const textHeight = element.offsetHeight;
  //     const startHeight = this.getHeightForTime(activity.time.startDate);
  //     const endHeight = this.getHeightForTime(activity.time.endDate);
  //     bottom = (startHeight - endHeight - 15) / 2 - textHeight / 2;
  //     if (startHeight - bottom > 960) {
  //       bottom = startHeight - 960;
  //       if ((bottom + textHeight) > (startHeight - endHeight - 15) - 30) {
  //         bottom = (startHeight - endHeight - 15) - textHeight - 15;
  //       }
  //     }
  //   }
  //   return  bottom + 'px';
  // }
}
