import { Component, OnInit } from '@angular/core';
import {AppComponent} from '../../app.component';
import {ActivityHandler} from '../../classes/handlers/activity-handler';
import {Activity} from '../../interfaces/activity';
import {TimeHandler} from '../../classes/handlers/time-handler';
import {LanguageHandler} from '../../classes/handlers/language-handler';
import {Globals} from '../../classes/globals';
import {OverlayHandler} from '../../classes/handlers/overlay-handler';
import {LayoutHandler} from '../../classes/handlers/layout-handler';

@Component({
  selector: 'app-vertical',
  templateUrl: './vertical.component.html',
  styleUrls: ['./vertical.component.css'],
  providers: [AppComponent]
})
export class VerticalComponent implements OnInit {

  activitySize = 150;
  prevActivity: Activity;
  prevActivityY: number;
  requestTime: number;

  constructor(private activityhandler: ActivityHandler,
              private timehandler: TimeHandler,
              private languaguehandler: LanguageHandler,
              private overlayhandler: OverlayHandler,
              private globals: Globals,
              private layouthandler: LayoutHandler) {
    this.prevActivity = null;
    this.prevActivityY = 5;
    this.requestTime = 0;
  }

  ngOnInit() {
  }

  hideActivity(activity: Activity) {
    const midLocation = this.getMidLocationActivity(activity);
    if (midLocation + 120 >= 1080) {
      return true;
    }
    return false;
  }

  getTopIcon(activity: Activity) {
    return (this.getMidLocationActivity(activity) - 50) + 'px';
  }

  getTopTime(activity: Activity) {
    return (this.getMidLocationActivity(activity) - 50) + 'px';
  }

  getTopTitle(activity: Activity) {
    return (this.getMidLocationActivity(activity) - 90) + 'px';
  }

  getTopLine(activity: Activity) {
    return (this.getMidLocationActivity(activity) + 4) + 'px';
  }

  getMidLocationActivity(activity: Activity) {
    // If equal then the other elements are checked, just return the known Y
    if (this.prevActivity === activity && this.requestTime < 4) {
      this.requestTime++;
      const result = this.prevActivityY;
      return Math.ceil(result);
    } else {
      this.requestTime = 0;
    }
    // If no prevActivity is known, you can just set this activities size
    // Or if the startTime of this activity is before the prevActivity we start over in rendering
    let prevActivityMinutes = null;
    if (this.prevActivity !== null) {
      prevActivityMinutes = this.prevActivity.startTime.hour * 60 + this.prevActivity.startTime.minute;
    }
    const activityMinutes = activity.startTime.hour * 60 + activity.startTime.minute;
    if (prevActivityMinutes === null || prevActivityMinutes >= activityMinutes) {
      let l;
      const currentMinutes = this.timehandler.getDate().getHours() * 60 + this.timehandler.getDate().getMinutes();
      if (activityMinutes <= currentMinutes) {
        l = (49 + this.activitySize / 2);
      } else {
        l = 49 + 900 / this.timehandler.lookForwardTime *
        (activityMinutes - currentMinutes) * 60 * 1000 + this.activitySize / 2;
      }
      this.prevActivityY = l;
      this.prevActivity = activity;
      return Math.ceil(l);
    }
    // Otherwise the activity needs to be set relative to the previous activity
    const prevActivityEndMinutes = this.prevActivity.endTime.hour * 60 + this.prevActivity.endTime.minute;
    const minDistance = this.activitySize + 75;
    const trueDifference = 900 / this.timehandler.lookForwardTime *
                           (activityMinutes - prevActivityEndMinutes) * 60 * 1000 + this.activitySize / 2;
    const difference = Math.max(minDistance, trueDifference);
    const location = this.prevActivityY + difference;
    this.prevActivity = activity;
    this.prevActivityY = location;
    return Math.ceil(location);
  }

  // TODO u should be language independent
  getStartTimeRepresentation(millis: number) {
    // Define from or till
    const prefix = this.languaguehandler.getTerm('from') + ' ';

    // Get date and return result (with prefix)
    const date = new Date(millis);
    if (millis <= this.timehandler.getDate().getTime()) {
      return prefix + this.languaguehandler.getTerm('now').toUpperCase();
    }
    let timeRepresentation = 12 + this.languaguehandler.getTerm('hourShort');
    if (date.getHours() !== 12) {
      timeRepresentation = (date.getHours() % 12) + this.languaguehandler.getTerm('hourShort');
    }
    if (date.getMinutes() !== 0) {
      timeRepresentation = ' ' + timeRepresentation + date.getMinutes();
    }
    return prefix + timeRepresentation;
  }

  getEndTimeRepresentation(millis: number) {
    // Define from or till
    const prefix = this.languaguehandler.getTerm('till') + ' ';

    // Get date and return result (with prefix)
    const date = new Date(millis);
    let timeRepresentation = 12 + this.languaguehandler.getTerm('hourShort');
    if (date.getHours() !== 12) {
      timeRepresentation = (date.getHours() % 12) + this.languaguehandler.getTerm('hourShort');
    }
    if (date.getMinutes() !== 0) {
      timeRepresentation = ' ' + timeRepresentation + date.getMinutes();
    }
    return prefix + timeRepresentation;
  }


  getColorForActivityBorder(activity: Activity) {
    if (activity.startTime.millis <= this.timehandler.getDate().getTime()) {
      return '#f2bf18';
    } else {
      return '#795548';
    }

  }

  hiddenEmpty() {
    if (this.activityhandler.getActivitiesToDisplay().length === 0) {
      return false;
    }
    return true;
  }

}
