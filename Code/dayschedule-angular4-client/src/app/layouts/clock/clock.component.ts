import {Component, OnInit} from '@angular/core';
import {Coordinates} from '../../classes/coordinates';
import {Activity} from '../../interfaces/activity';
import {TimeHandler} from '../../classes/handlers/time-handler';
import {ActivityHandler} from '../../classes/handlers/activity-handler';
import {AppComponent} from '../../app.component';

// Afwerking
// TODO gepersonaliseerde foto's en berichtjes (in widgets)


// TODO's NIET NODIG VOOR ONTMOETINGSHUIS
// - night weather icons
// - keep a list of persons somewhere and refer to it.
// FIXME ExpressionChangedAfterItHasBeenCheckedError

@Component({
  selector: 'app-clock',
  templateUrl: './clock.component.html',
  styleUrls: ['./clock.component.css', '../../app.component.css'],
  providers: [AppComponent]
})
export class ClockComponent implements OnInit {

  clockWidthHeight: number;
  textIconInformationScaling: number;

  constructor(private timehandler: TimeHandler,
              private activityhandler: ActivityHandler) {
  }

  ngOnInit() {
    // Init variables
    this.clockWidthHeight = 1000;
    this.textIconInformationScaling = 0.3;
  }

  getHalfAngle(activity) {
    let startDateAngle = (new Date(activity.startDate).getMinutes() +
      (new Date(activity.startDate).getHours() % 12) * 60) / 2 - 90;
    let endDateAngle = (new Date(activity.endDate).getMinutes() +
      (new Date(activity.endDate).getHours() % 12) * 60) / 2 - 90;
    while (startDateAngle < 0) {
      startDateAngle += 360;
    }
    while (endDateAngle < 0) {
      endDateAngle += 360;
    }
    if (startDateAngle + endDateAngle === 360 && startDateAngle > 270) {
      return 0;
    }
    return (startDateAngle + endDateAngle) / 2;
  }

  getIconX(activity) {
    const halfAngle = this.getHalfAngle(activity);
    return this.getX(halfAngle, this.clockWidthHeight / 2, this.clockWidthHeight / 2 * this.textIconInformationScaling);
  }

  getIconY(activity) {
    const halfAngle = this.getHalfAngle(activity);
    return this.getY(halfAngle, this.clockWidthHeight / 2, this.clockWidthHeight / 2 * this.textIconInformationScaling);
  }

  getPersonX(activity) {
    const halfAngle = this.getHalfAngle(activity);
    return this.getX(halfAngle, this.clockWidthHeight / 2, this.clockWidthHeight / 2 * this.textIconInformationScaling - 0.15);
  }

  getPersonY(activity) {
    const halfAngle = this.getHalfAngle(activity);
    return this.getY(halfAngle, this.clockWidthHeight / 2, this.clockWidthHeight / 2 * this.textIconInformationScaling - 0.15);
  }

  getPastSlice() {
    // x = cx + r * cos(a)
    // y = cy + r * sin(a)
    const c = this.getSliceCoordinatesCurrentTime();
    const middle = 'M' + c.cx + ',' + c.cy;
    const lineUp = 'L' + c.lx + ',' + c.ly;
    const arc = 'A' + c.r + ',' + c.r + ' 1 0,1 ' + c.ax + ',' + c.ay;

    return middle + ' ' + lineUp + ' ' + arc + ' Z';
  }

  getCircleSlice(activity) {
    const r = this.clockWidthHeight / 2 - 10;
    // x = cx + r * cos(a)
    // y = cy + r * sin(a)
    const c = this.getSliceCoordinates(activity, r);
    const middle = 'M' + c.cx + ',' + c.cy;
    const lineUp = 'L' + c.lx + ',' + c.ly;
    const arc = 'A' + c.r + ',' + c.r + ' 1 0,1 ' + c.ax + ',' + c.ay;

    return middle + ' ' + lineUp + ' ' + arc + ' Z';
  }

  getSliceCoordinates(activity, r) {
    const cx = this.clockWidthHeight / 2;
    const cy = this.clockWidthHeight / 2;

    const startDateAngle = (new Date(activity.startDate).getMinutes() +
      (new Date(activity.startDate).getHours() % 12) * 60) / 2 - 90;
    const endDateAngle = (new Date(activity.endDate).getMinutes() +
      (new Date(activity.endDate).getHours() % 12) * 60) / 2 - 90;

    const lx = this.getX(startDateAngle, cx, r);
    const ly = this.getY(startDateAngle, cy, r);

    const ax = this.getX(endDateAngle, cx, r);
    const ay = this.getY(endDateAngle, cy, r);

    return new Coordinates(cx, cy, r, lx, ly, ax, ay);
  }

  getSliceCoordinatesCurrentTime() {
    const cx = this.clockWidthHeight / 2;
    const cy = this.clockWidthHeight / 2;
    const r = this.clockWidthHeight / 2;

    const endDateAngle = this.getCurrentAngle();
    const startDateAngle = endDateAngle - Math.floor((12 - this.timehandler.lookForwardTime) / 1000 / 60 / 60 * 30);

    const lx = this.getX(startDateAngle, cx, r);
    const ly = this.getY(startDateAngle, cy, r);

    const ax = this.getX(endDateAngle, cx, r);
    const ay = this.getY(endDateAngle, cy, r);

    return new Coordinates(cx, cy, r, lx, ly, ax, ay);
  }

  getFadeX1() {
    const rotation = (360 - this.getCurrentAngle() + 40) % 360;
    const pi = rotation * (Math.PI / 180);
    return Math.round(50 + Math.sin(pi) * 50) + '%';
  }

  getFadeY1() {
    const rotation = (360 - this.getCurrentAngle() + 40) % 360;
    const pi = rotation * (Math.PI / 180);
    return Math.round(50 + Math.cos(pi) * 50) + '%';
  }

  getFadeX2() {
    const rotation = (360 - this.getCurrentAngle() + 40) % 360;
    const pi = rotation * (Math.PI / 180);
    return Math.round(50 + Math.sin(pi + Math.PI) * 50) + '%';
  }

  getFadeY2() {
    const rotation = (360 - this.getCurrentAngle() + 40) % 360;
    const pi = rotation * (Math.PI / 180);
    return Math.round(50 + Math.cos(pi + Math.PI) * 50) + '%';
  }

  getCurrentAngle() {
    const result = ((this.timehandler.getDate().getMinutes() +
      (this.timehandler.getDate().getHours() % 12) * 60) / 2 - 90) % 360;
    return result;
  }

  getTimeX() {
    const angle = (this.timehandler.getDate().getMinutes() + (this.timehandler.getDate().getHours() % 12) * 60) / 2 - 90;
    return this.getX(angle, this.clockWidthHeight / 2, this.clockWidthHeight / 2 - 20);
  }

  getTimeY() {
    const angle = (this.timehandler.getDate().getMinutes() + (this.timehandler.getDate().getHours() % 12) * 60) / 2 - 90;
    return this.getY(angle, this.clockWidthHeight / 2, this.clockWidthHeight / 2 - 20);
  }

  getX(angle, cx, r) {
    return Math.floor(cx + r * Math.cos(angle * (Math.PI / 180)));
  }

  getY(angle, cy, r) {
    return Math.floor(cy + r * Math.sin(angle * (Math.PI / 180)));
  }

  getTextSquareCircleMethod(activity: Activity) {
    const coordinates = this.getSliceCoordinates(activity, this.clockWidthHeight / 2);
    const mx = (coordinates.cx + coordinates.lx + coordinates.ax) / 3;
    const my = (coordinates.cy + coordinates.ly + coordinates.ay) / 3;

    const a = Math.sqrt(Math.pow(coordinates.cx - coordinates.lx, 2) + Math.pow(coordinates.cy - coordinates.ly, 2));
    const b = Math.sqrt(Math.pow(coordinates.cx - coordinates.ax, 2) + Math.pow(coordinates.cy - coordinates.ay, 2));
    const c = Math.sqrt(Math.pow(coordinates.ax - coordinates.lx, 2) + Math.pow(coordinates.ay - coordinates.ly, 2));
    const k = (a + b + c) / 2;
    const radius = Math.sqrt(k * (k - a) * (k - b) * (k - c)) / k;
    const width = Math.sqrt(2) * radius;
    const height = width;
    const left = mx - width / 2;
    const top = my - height / 2;
    return {width: width + 'px', height: height + 'px', top: top + 'px', left: left + 'px'};
  }

  getActivityTitle(activity: Activity) {
    if (activity.overlapping === 0) {
      return activity.title;
    } else {
      if (activity.calendarName !== this.activityhandler.calendarName
        && activity.overlappingActivity.calendarName === this.activityhandler.calendarName) {
        return activity.overlappingActivity.title;
      } else if (activity.overlappingActivity.calendarName !== this.activityhandler.calendarName
        && activity.calendarName === this.activityhandler.calendarName) {
        return activity.title;
      } else {
        let t1 = '';
        if (this.activityhandler.getTimeStamp(activity).length !== 0) {
          t1 = ' (' + this.activityhandler.getTimeStamp(activity) + ')';
        }

        let t2 = '';
        if (this.activityhandler.getTimeStamp(activity.overlappingActivity).length !== 0) {
          t2 = ' (' + this.activityhandler.getTimeStamp(activity.overlappingActivity) + ')';
        }
        return activity.title + t1 + ' & ' + activity.overlappingActivity.title + t2;
      }
    }
  }
}


