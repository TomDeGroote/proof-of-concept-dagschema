import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { ClockComponent } from './layouts/clock/clock.component';
import { CalendarOverlayComponent } from './overlay/calendar-overlay/calendar-overlay.component';
import { PicturesOverlayComponent } from './overlay/pictures-overlay/pictures-overlay.component';
import { ActivityOverlayComponent } from './overlay/activity-overlay/activity-overlay.component';
import { WalkComponent } from './layouts/walk/walk.component';
import { TimeWidgetComponent } from './widgets/time-widget/time-widget.component';
import { OverlayComponent } from './overlay/overlay.component';
import { CalendarWidgetComponent } from './widgets/calendar-widget/calendar-widget.component';
import { PicturesWidgetComponent } from './widgets/pictures-widget/pictures-widget.component';
import { WeatherWidgetComponent } from './widgets/weather-widget/weather-widget.component';
import { VolunteerWidgetComponent } from './widgets/volunteer-widget/volunteer-widget.component';
import { VerticalComponent } from './layouts/vertical/vertical.component';
import { CaregiverWidgetComponent } from './widgets/caregiver-widget/caregiver-widget.component';

declare var $: any;

@NgModule({
  declarations: [
    AppComponent,
    ClockComponent,
    CalendarOverlayComponent,
    PicturesOverlayComponent,
    ActivityOverlayComponent,
    WalkComponent,
    TimeWidgetComponent,
    OverlayComponent,
    CalendarWidgetComponent,
    PicturesWidgetComponent,
    WeatherWidgetComponent,
    VolunteerWidgetComponent,
    VerticalComponent,
    CaregiverWidgetComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
