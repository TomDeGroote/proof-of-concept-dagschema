import {Component, Input, OnInit} from '@angular/core';
import {Activity} from '../../interfaces/activity';
import {HttpClientHandler} from '../../classes/handlers/http-client-handler';
import {LayoutHandler} from '../../classes/handlers/layout-handler';
import {Globals} from '../../classes/globals';
import {AppComponent} from '../../app.component';
import {ActivityHandler} from '../../classes/handlers/activity-handler';

@Component({
  selector: 'app-activity-overlay',
  templateUrl: './activity-overlay.component.html',
  styleUrls: ['./activity-overlay.component.css'],
  providers: [AppComponent]
})
export class ActivityOverlayComponent implements OnInit {

  @Input() activity: Activity;

  constructor(private httpclienthandler: HttpClientHandler,
              private layouthandler: LayoutHandler,
              private globals: Globals,
              private activityhandler: ActivityHandler) { }

  ngOnInit() {
  }

}
