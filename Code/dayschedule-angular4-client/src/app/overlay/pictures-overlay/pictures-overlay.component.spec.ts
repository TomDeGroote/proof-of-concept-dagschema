import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PicturesOverlayComponent } from './pictures-overlay.component';

describe('PicturesOverlayComponent', () => {
  let component: PicturesOverlayComponent;
  let fixture: ComponentFixture<PicturesOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PicturesOverlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PicturesOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
