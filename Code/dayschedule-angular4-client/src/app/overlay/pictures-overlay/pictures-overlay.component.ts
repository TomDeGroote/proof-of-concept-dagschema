import {Component, OnInit} from '@angular/core';
import {LayoutHandler} from '../../classes/handlers/layout-handler';
import {AppComponent} from '../../app.component';
import {Globals} from '../../classes/globals';
import {OverlayHandler} from '../../classes/handlers/overlay-handler';

@Component({
  selector: 'app-pictures-overlay',
  templateUrl: './pictures-overlay.component.html',
  styleUrls: ['./pictures-overlay.component.css'],
  providers: [AppComponent]
})
export class PicturesOverlayComponent implements OnInit {

  constructor(private layouthandler: LayoutHandler,
              private globals: Globals,
              private overlayhandler: OverlayHandler) {}

  ngOnInit() {
  }

}
