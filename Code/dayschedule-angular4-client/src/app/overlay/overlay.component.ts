import {Component, OnInit} from '@angular/core';
import {OverlayHandler} from '../classes/handlers/overlay-handler';
import {AppComponent} from '../app.component';
import {LayoutHandler} from '../classes/handlers/layout-handler';
import {Globals} from '../classes/globals';
import {ActivityHandler} from '../classes/handlers/activity-handler';
import {LanguageHandler} from '../classes/handlers/language-handler';
import {HttpClientHandler} from '../classes/handlers/http-client-handler';

@Component({
  selector: 'app-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.css'],
  providers: [AppComponent]
})
export class OverlayComponent implements OnInit {

  constructor(private overlayhandler: OverlayHandler,
              private layouthandler: LayoutHandler,
              private globals: Globals,
              private activityhandler: ActivityHandler,
              private languagehandler: LanguageHandler,
              private httpclienthandler: HttpClientHandler) { }

  ngOnInit() {
    this.overlayhandler.overlayDisplay = 'none';
  }

  hideOverlayBack() {
    this.httpclienthandler.log('CLICK -> hiding overlay: clicked back button');
    this.overlayhandler.overlayApp = 'none';
    this.overlayhandler.overlayDisplay = 'none';
    this.overlayhandler.overlayApp = 'none';
  }

  hideOverlayNextTo() {
    this.httpclienthandler.log('CLICK -> hiding overlay: clicked next to overlay');
    this.overlayhandler.overlayApp = 'none';
    this.overlayhandler.overlayDisplay = 'none';
    this.overlayhandler.overlayApp = 'none';
  }

}
