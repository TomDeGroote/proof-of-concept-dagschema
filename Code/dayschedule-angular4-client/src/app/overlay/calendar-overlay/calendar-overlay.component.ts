import {Component, OnInit} from '@angular/core';
import {Activity} from '../../interfaces/activity';
import {TimeHandler} from '../../classes/handlers/time-handler';
import {ActivityHandler} from '../../classes/handlers/activity-handler';
import {AppComponent} from '../../app.component';
import {Globals} from '../../classes/globals';
import {LayoutHandler} from '../../classes/handlers/layout-handler';
import {LanguageHandler} from '../../classes/handlers/language-handler';

@Component({
  selector: 'app-calendar-overlay',
  templateUrl: './calendar-overlay.component.html',
  styleUrls: ['./calendar-overlay.component.css'],
  providers: [AppComponent]
})
export class CalendarOverlayComponent implements OnInit {

  constructor(private timehandler: TimeHandler,
              private activityhandler: ActivityHandler,
              private globals: Globals,
              private layouthandler: LayoutHandler,
              private languagehandler: LanguageHandler) {}

  widths = [209, 141, 140, 208, 209, 138, 139];
  lefts = [162, 374, 518, 661, 874, 1087, 1230];


  ngOnInit() {
  }

  getYear() {
    return this.timehandler.getDate().getFullYear();
  }

  getWidth(activity: Activity, day: number) {
    let width = this.widths[day];
    if (this.activityhandler.exceptionActivities.indexOf(activity.title) >= 0) {
      return width + 'px';
    }
    if (activity.overlapping > 0) {
      width = width / 2 - 1;
    }
    return width + 'px';
  }

  getLeft(activity: Activity, day: number) {
    let left = this.lefts[day];
    if (activity.overlapping > 1) {
      left += this.widths[day] / 2 + 1;
    }
    return left + 'px';
  }

  getTop(activity: Activity) {
    // TODO kan efficiënter
    // TODO kan maar 1 avondactiviteit aan!
    // TODO buggy voor activiteiten voor 8u!
    let location = activity.startTime.hour * 60 + activity.startTime.minute;
    if (location < 8 * 60) {
      location = 8 * 60;
    }
    if (location >= 17 * 60) {
      location = 17 * 60;
    }
    const top = 200 + 74 * (location - 8 * 60) / 60;
    return top + 'px';
  }

  getHeight(activity: Activity) {
    let height = 74 * (activity.endTime.millis - activity.startTime.millis) / 1000 / 60 / 60;
    const location = activity.startTime.hour * 60 + activity.startTime.minute;
    if (location >= 17 * 60) {
      height = 89;
    } else {
      const endLocation = activity.endTime.hour * 60 + activity.endTime.minute;
      if (activity.startTime.hour < 8) {
        height = 74 * ((17 - 8) * 60) / 60 + 88;

      } else if (endLocation > 17 * 60) {
        height = 74 * ((17 - activity.startTime.hour) * 60 - activity.startTime.minute) / 60 + 88;
      }
    }
    return height - 4 + 'px';
  }

  getBackgroundColor(activity: Activity) {
    if (activity.calendarName === this.activityhandler.calendarName) {
      return '#f2c42f';
    } else {
      if (activity.color === '4') {
        return '#ff63b1';
      }
      if (activity.color === '7') {
        return '#44d9e6';
      }
    }
  }

  getLeftMarker() {
    let day = this.timehandler.getDate().getDay() - 1;
    if (day === -1) {
      day = 6;
    }
    return (this.lefts[day] + this.widths[day] / 2 - 15) + 'px';
  }

  getLeftNow() {
    let day = this.timehandler.getDate().getDay() - 1;
    if (day === -1) {
      day = 6;
    }
    return this.lefts[day] + 'px';
  }

  getWidthNow() {
    let day = this.timehandler.getDate().getDay() - 1;
    if (day === -1) {
      day = 6;
    }
    return this.widths[day] + 'px';
  }

  getTopNow() {
    const hours = this.timehandler.getDate().getHours();
    if (hours < 8) {
      return '280px';
    }
    const location = 197 + ((hours - 8) * 60 + this.timehandler.getDate().getMinutes()) * 74 / 60;
    return location + 'px';
  }

  hideNowLine() {
    if (this.timehandler.getDate().getHours() > 17 || this.timehandler.getDate().getHours() < 8) {
      return true;
    }
    return false;
  }
}
