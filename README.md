# Ondersteunen van de actieve levensstijl van mensen met jongdementie.

## Uitleg
Deze repository bevat:

- De demo -> dayschedule-0.2.6.jar
- De code -> Code/

De demo is ontwikkeld voor een specifieke resolutie (1920x1080) op Linux voor Firefox. Bij andere resoluties, besturingssystemen of browsers zal het optimale resultaat mogelijks niet zichtbaar zijn. Bij een kleinere resolutie zal de plaatsing van de verschillende aspecten van het dagschema foutief zijn, de functionaliteiten zouden echter nog moeten werken.
Daarnaast zal op de kalender te zien zijn: "Er is niets meer gepland vandaag" en de status "Ik ben thuis" omdat de jar geen toegang heeft tot de nodige Google kalenders.

De demo maakt daarnaast gebruik van poort 8080, deze moet dus vrij zijn voor de demo om te werken.

## Getest op
- Besturingssysteem: Linux (Raspbian, Arch, Ubuntu), Windows
- Resolutie scherm: 1920x1080
- Webbrowser: Firefox
- Java versie: 1.8.0_172

## De demo runnen
Om de demo te runnen heb je enkel de jar op deze repository nodig.

1. Clone/download de repository
2. Run dayschedule-0.2.6.jar (bv. met java -jar dayschedule-0.2.6.jar)
3. Na enkele seconden, afhankelijk van de snelheid van de gebruikte computer, is de server opgestart
4. Open een webbrowser
5. Surf naar localhost:8080 (als de poort niet vrij is wordt een error gegeven bij het opstarten van de jar)
6. In uw browser ziet u nu normaal de demo. Voor het optimale resultaat wordt de demo best fullscreen gezet. In Firefox kan dit met bv. F11.

## Uitleg code
De code is functioneel geschreven. Hiermee wordt bedoeld dat een specifiek doel moest worden bereikt om de thuistesten uit te kunnen voeren en dat de code hiernaar geschreven is. Ze is dus niet noodzakelijk makkelijk uitbreidbaar of compatibel met verschillende besturingssystemen.

In tegenstelling tot de jar, probeert de code wel nog met de Google Calendar API te connecteren. Dit zal echter niet lukken aangezien onder meer de credentials niet op deze git staan (voor veiligheidsredenen). De code zoals hier ter beschikking gesteld dient dus louter als illustratie, voor het runnen van de demo kan de jar worden gebruikt.

De code folder bevat:

- dayschedule-springboot-server -> de server van het programma. De java main klasse is DayscheduleApplication.
- dayschedule-angular4-client -> De main html staat in src/app/app.component.html. 

